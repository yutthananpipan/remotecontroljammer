﻿namespace drc
{
    partial class ShutdownControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelShutdown = new System.Windows.Forms.Panel();
            this.panelSwitch = new System.Windows.Forms.Panel();
            this.ShutdownIMG = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.charge_mode = new System.Windows.Forms.Label();
            this.panelname = new System.Windows.Forms.Panel();
            this.labelname = new System.Windows.Forms.Label();
            this.panelShutdown.SuspendLayout();
            this.panelSwitch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShutdownIMG)).BeginInit();
            this.panel1.SuspendLayout();
            this.panelname.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelShutdown
            // 
            this.panelShutdown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.panelShutdown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelShutdown.Controls.Add(this.panelSwitch);
            this.panelShutdown.Controls.Add(this.panel1);
            this.panelShutdown.Controls.Add(this.panelname);
            this.panelShutdown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelShutdown.Location = new System.Drawing.Point(0, 0);
            this.panelShutdown.Name = "panelShutdown";
            this.panelShutdown.Size = new System.Drawing.Size(234, 194);
            this.panelShutdown.TabIndex = 4;
            // 
            // panelSwitch
            // 
            this.panelSwitch.BackColor = System.Drawing.Color.Transparent;
            this.panelSwitch.Controls.Add(this.ShutdownIMG);
            this.panelSwitch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSwitch.Location = new System.Drawing.Point(0, 50);
            this.panelSwitch.Name = "panelSwitch";
            this.panelSwitch.Padding = new System.Windows.Forms.Padding(60, 25, 25, 25);
            this.panelSwitch.Size = new System.Drawing.Size(189, 144);
            this.panelSwitch.TabIndex = 10;
            // 
            // ShutdownIMG
            // 
            this.ShutdownIMG.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ShutdownIMG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ShutdownIMG.Image = global::drc.Properties.Resources.btn_shutdown;
            this.ShutdownIMG.Location = new System.Drawing.Point(60, 25);
            this.ShutdownIMG.Name = "ShutdownIMG";
            this.ShutdownIMG.Size = new System.Drawing.Size(104, 94);
            this.ShutdownIMG.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ShutdownIMG.TabIndex = 0;
            this.ShutdownIMG.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.charge_mode);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(189, 50);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 15, 0, 0);
            this.panel1.Size = new System.Drawing.Size(45, 144);
            this.panel1.TabIndex = 9;
            // 
            // charge_mode
            // 
            this.charge_mode.AutoSize = true;
            this.charge_mode.Dock = System.Windows.Forms.DockStyle.Top;
            this.charge_mode.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.charge_mode.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.charge_mode.Location = new System.Drawing.Point(0, 15);
            this.charge_mode.Name = "charge_mode";
            this.charge_mode.Size = new System.Drawing.Size(0, 15);
            this.charge_mode.TabIndex = 0;
            // 
            // panelname
            // 
            this.panelname.BackColor = System.Drawing.Color.Transparent;
            this.panelname.Controls.Add(this.labelname);
            this.panelname.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelname.Location = new System.Drawing.Point(0, 0);
            this.panelname.Name = "panelname";
            this.panelname.Padding = new System.Windows.Forms.Padding(5);
            this.panelname.Size = new System.Drawing.Size(234, 50);
            this.panelname.TabIndex = 7;
            // 
            // labelname
            // 
            this.labelname.AutoSize = true;
            this.labelname.Font = new System.Drawing.Font("Segoe UI", 11.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelname.ForeColor = System.Drawing.Color.White;
            this.labelname.Location = new System.Drawing.Point(63, 7);
            this.labelname.Name = "labelname";
            this.labelname.Size = new System.Drawing.Size(107, 28);
            this.labelname.TabIndex = 4;
            this.labelname.Text = "Shutdown";
            // 
            // ShutdownControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelShutdown);
            this.Name = "ShutdownControl";
            this.Size = new System.Drawing.Size(234, 194);
            this.Load += new System.EventHandler(this.ShutdownControl1_Load);
            this.Resize += new System.EventHandler(this.ShutdownControl1_Resize);
            this.panelShutdown.ResumeLayout(false);
            this.panelSwitch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ShutdownIMG)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelname.ResumeLayout(false);
            this.panelname.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelShutdown;
        private System.Windows.Forms.Panel panelname;
        private System.Windows.Forms.Label labelname;
        private System.Windows.Forms.Panel panelSwitch;
        private System.Windows.Forms.PictureBox ShutdownIMG;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label charge_mode;
    }
}
