﻿namespace drc
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panelBackground = new System.Windows.Forms.Panel();
            this.panelCenter = new System.Windows.Forms.Panel();
            this.panel10Meter = new System.Windows.Forms.Panel();
            this.tableLayoutPanel12Meter = new System.Windows.Forms.TableLayoutPanel();
            this.PanelGauge15 = new System.Windows.Forms.Panel();
            this.PanelGauge1 = new System.Windows.Forms.Panel();
            this.PanelGauge2 = new System.Windows.Forms.Panel();
            this.PanelGauge3 = new System.Windows.Forms.Panel();
            this.PanelGauge4 = new System.Windows.Forms.Panel();
            this.PanelGauge5 = new System.Windows.Forms.Panel();
            this.PanelGauge6 = new System.Windows.Forms.Panel();
            this.PanelGauge7 = new System.Windows.Forms.Panel();
            this.PanelGauge8 = new System.Windows.Forms.Panel();
            this.PanelGauge9 = new System.Windows.Forms.Panel();
            this.PanelGauge10 = new System.Windows.Forms.Panel();
            this.PanelGauge11 = new System.Windows.Forms.Panel();
            this.PanelGauge12 = new System.Windows.Forms.Panel();
            this.PanelGauge13 = new System.Windows.Forms.Panel();
            this.PanelGauge14 = new System.Windows.Forms.Panel();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.panelPercent = new System.Windows.Forms.Panel();
            this.tableLayoutPercent = new System.Windows.Forms.TableLayoutPanel();
            this.panelPercenBatterPC = new System.Windows.Forms.Panel();
            this.panelPercenBatt = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LabelnameBatteryStatus = new System.Windows.Forms.Label();
            this.tableLayoutPanelToggle = new System.Windows.Forms.TableLayoutPanel();
            this.panelAntenna = new System.Windows.Forms.Panel();
            this.panelShutdown = new System.Windows.Forms.Panel();
            this.panelSwitch = new System.Windows.Forms.Panel();
            this.tableLayoutCharger = new System.Windows.Forms.TableLayoutPanel();
            this.panelAmp = new System.Windows.Forms.Panel();
            this.panelVolt = new System.Windows.Forms.Panel();
            this.panelHead = new System.Windows.Forms.Panel();
            this.panelName = new System.Windows.Forms.Panel();
            this.brandname = new System.Windows.Forms.Label();
            this.panelClock = new System.Windows.Forms.Panel();
            this.labelDititalClock = new System.Windows.Forms.Label();
            this.panelLogo = new System.Windows.Forms.Panel();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.timer_maindesktop = new System.Windows.Forms.Timer(this.components);
            this.panelBackground.SuspendLayout();
            this.panelCenter.SuspendLayout();
            this.panel10Meter.SuspendLayout();
            this.tableLayoutPanel12Meter.SuspendLayout();
            this.panelBottom.SuspendLayout();
            this.panelPercent.SuspendLayout();
            this.tableLayoutPercent.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanelToggle.SuspendLayout();
            this.tableLayoutCharger.SuspendLayout();
            this.panelHead.SuspendLayout();
            this.panelName.SuspendLayout();
            this.panelClock.SuspendLayout();
            this.panelLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // panelBackground
            // 
            this.panelBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(84)))), ((int)(((byte)(100)))));
            this.panelBackground.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelBackground.Controls.Add(this.panelCenter);
            this.panelBackground.Controls.Add(this.panelHead);
            this.panelBackground.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBackground.Location = new System.Drawing.Point(0, 0);
            this.panelBackground.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelBackground.Name = "panelBackground";
            this.panelBackground.Size = new System.Drawing.Size(1942, 1102);
            this.panelBackground.TabIndex = 1;
            // 
            // panelCenter
            // 
            this.panelCenter.BackColor = System.Drawing.Color.Transparent;
            this.panelCenter.Controls.Add(this.panel10Meter);
            this.panelCenter.Controls.Add(this.panelBottom);
            this.panelCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCenter.Location = new System.Drawing.Point(0, 51);
            this.panelCenter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelCenter.Name = "panelCenter";
            this.panelCenter.Size = new System.Drawing.Size(1942, 1051);
            this.panelCenter.TabIndex = 2;
            // 
            // panel10Meter
            // 
            this.panel10Meter.BackColor = System.Drawing.Color.Transparent;
            this.panel10Meter.Controls.Add(this.tableLayoutPanel12Meter);
            this.panel10Meter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10Meter.Location = new System.Drawing.Point(0, 0);
            this.panel10Meter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel10Meter.Name = "panel10Meter";
            this.panel10Meter.Size = new System.Drawing.Size(1942, 852);
            this.panel10Meter.TabIndex = 4;
            // 
            // tableLayoutPanel12Meter
            // 
            this.tableLayoutPanel12Meter.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel12Meter.ColumnCount = 5;
            this.tableLayoutPanel12Meter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12Meter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12Meter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12Meter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12Meter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel12Meter.Controls.Add(this.PanelGauge15, 4, 2);
            this.tableLayoutPanel12Meter.Controls.Add(this.PanelGauge1, 0, 0);
            this.tableLayoutPanel12Meter.Controls.Add(this.PanelGauge2, 1, 0);
            this.tableLayoutPanel12Meter.Controls.Add(this.PanelGauge3, 2, 0);
            this.tableLayoutPanel12Meter.Controls.Add(this.PanelGauge4, 3, 0);
            this.tableLayoutPanel12Meter.Controls.Add(this.PanelGauge5, 4, 0);
            this.tableLayoutPanel12Meter.Controls.Add(this.PanelGauge6, 0, 1);
            this.tableLayoutPanel12Meter.Controls.Add(this.PanelGauge7, 1, 1);
            this.tableLayoutPanel12Meter.Controls.Add(this.PanelGauge8, 2, 1);
            this.tableLayoutPanel12Meter.Controls.Add(this.PanelGauge9, 3, 1);
            this.tableLayoutPanel12Meter.Controls.Add(this.PanelGauge10, 4, 1);
            this.tableLayoutPanel12Meter.Controls.Add(this.PanelGauge11, 0, 2);
            this.tableLayoutPanel12Meter.Controls.Add(this.PanelGauge12, 1, 2);
            this.tableLayoutPanel12Meter.Controls.Add(this.PanelGauge13, 2, 2);
            this.tableLayoutPanel12Meter.Controls.Add(this.PanelGauge14, 3, 2);
            this.tableLayoutPanel12Meter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12Meter.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel12Meter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel12Meter.Name = "tableLayoutPanel12Meter";
            this.tableLayoutPanel12Meter.RowCount = 3;
            this.tableLayoutPanel12Meter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel12Meter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel12Meter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel12Meter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12Meter.Size = new System.Drawing.Size(1942, 852);
            this.tableLayoutPanel12Meter.TabIndex = 2;
            // 
            // PanelGauge15
            // 
            this.PanelGauge15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.PanelGauge15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelGauge15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelGauge15.Location = new System.Drawing.Point(1555, 570);
            this.PanelGauge15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelGauge15.Name = "PanelGauge15";
            this.PanelGauge15.Size = new System.Drawing.Size(384, 280);
            this.PanelGauge15.TabIndex = 17;
            // 
            // PanelGauge1
            // 
            this.PanelGauge1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.PanelGauge1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelGauge1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelGauge1.Location = new System.Drawing.Point(3, 2);
            this.PanelGauge1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelGauge1.Name = "PanelGauge1";
            this.PanelGauge1.Size = new System.Drawing.Size(382, 280);
            this.PanelGauge1.TabIndex = 1;
            // 
            // PanelGauge2
            // 
            this.PanelGauge2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.PanelGauge2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelGauge2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelGauge2.Location = new System.Drawing.Point(391, 2);
            this.PanelGauge2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelGauge2.Name = "PanelGauge2";
            this.PanelGauge2.Size = new System.Drawing.Size(382, 280);
            this.PanelGauge2.TabIndex = 3;
            // 
            // PanelGauge3
            // 
            this.PanelGauge3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.PanelGauge3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelGauge3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelGauge3.Location = new System.Drawing.Point(779, 2);
            this.PanelGauge3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelGauge3.Name = "PanelGauge3";
            this.PanelGauge3.Size = new System.Drawing.Size(382, 280);
            this.PanelGauge3.TabIndex = 5;
            // 
            // PanelGauge4
            // 
            this.PanelGauge4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.PanelGauge4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelGauge4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelGauge4.Location = new System.Drawing.Point(1167, 2);
            this.PanelGauge4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelGauge4.Name = "PanelGauge4";
            this.PanelGauge4.Size = new System.Drawing.Size(382, 280);
            this.PanelGauge4.TabIndex = 4;
            // 
            // PanelGauge5
            // 
            this.PanelGauge5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.PanelGauge5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelGauge5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelGauge5.Location = new System.Drawing.Point(1555, 2);
            this.PanelGauge5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelGauge5.Name = "PanelGauge5";
            this.PanelGauge5.Size = new System.Drawing.Size(384, 280);
            this.PanelGauge5.TabIndex = 6;
            // 
            // PanelGauge6
            // 
            this.PanelGauge6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.PanelGauge6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelGauge6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelGauge6.Location = new System.Drawing.Point(3, 286);
            this.PanelGauge6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelGauge6.Name = "PanelGauge6";
            this.PanelGauge6.Size = new System.Drawing.Size(382, 280);
            this.PanelGauge6.TabIndex = 7;
            // 
            // PanelGauge7
            // 
            this.PanelGauge7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.PanelGauge7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelGauge7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelGauge7.Location = new System.Drawing.Point(391, 286);
            this.PanelGauge7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelGauge7.Name = "PanelGauge7";
            this.PanelGauge7.Size = new System.Drawing.Size(382, 280);
            this.PanelGauge7.TabIndex = 8;
            // 
            // PanelGauge8
            // 
            this.PanelGauge8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.PanelGauge8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelGauge8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelGauge8.Location = new System.Drawing.Point(779, 286);
            this.PanelGauge8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelGauge8.Name = "PanelGauge8";
            this.PanelGauge8.Size = new System.Drawing.Size(382, 280);
            this.PanelGauge8.TabIndex = 9;
            // 
            // PanelGauge9
            // 
            this.PanelGauge9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.PanelGauge9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelGauge9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelGauge9.Location = new System.Drawing.Point(1167, 286);
            this.PanelGauge9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelGauge9.Name = "PanelGauge9";
            this.PanelGauge9.Size = new System.Drawing.Size(382, 280);
            this.PanelGauge9.TabIndex = 10;
            // 
            // PanelGauge10
            // 
            this.PanelGauge10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.PanelGauge10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelGauge10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelGauge10.Location = new System.Drawing.Point(1555, 286);
            this.PanelGauge10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelGauge10.Name = "PanelGauge10";
            this.PanelGauge10.Size = new System.Drawing.Size(384, 280);
            this.PanelGauge10.TabIndex = 11;
            // 
            // PanelGauge11
            // 
            this.PanelGauge11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.PanelGauge11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelGauge11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelGauge11.Location = new System.Drawing.Point(3, 570);
            this.PanelGauge11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelGauge11.Name = "PanelGauge11";
            this.PanelGauge11.Size = new System.Drawing.Size(382, 280);
            this.PanelGauge11.TabIndex = 12;
            // 
            // PanelGauge12
            // 
            this.PanelGauge12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.PanelGauge12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelGauge12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelGauge12.Location = new System.Drawing.Point(391, 570);
            this.PanelGauge12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelGauge12.Name = "PanelGauge12";
            this.PanelGauge12.Size = new System.Drawing.Size(382, 280);
            this.PanelGauge12.TabIndex = 13;
            // 
            // PanelGauge13
            // 
            this.PanelGauge13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.PanelGauge13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelGauge13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelGauge13.Location = new System.Drawing.Point(779, 570);
            this.PanelGauge13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelGauge13.Name = "PanelGauge13";
            this.PanelGauge13.Size = new System.Drawing.Size(382, 280);
            this.PanelGauge13.TabIndex = 14;
            // 
            // PanelGauge14
            // 
            this.PanelGauge14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.PanelGauge14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelGauge14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelGauge14.Location = new System.Drawing.Point(1167, 570);
            this.PanelGauge14.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelGauge14.Name = "PanelGauge14";
            this.PanelGauge14.Size = new System.Drawing.Size(382, 280);
            this.PanelGauge14.TabIndex = 15;
            // 
            // panelBottom
            // 
            this.panelBottom.Controls.Add(this.panelPercent);
            this.panelBottom.Controls.Add(this.tableLayoutPanelToggle);
            this.panelBottom.Controls.Add(this.tableLayoutCharger);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 852);
            this.panelBottom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(1942, 199);
            this.panelBottom.TabIndex = 3;
            // 
            // panelPercent
            // 
            this.panelPercent.Controls.Add(this.tableLayoutPercent);
            this.panelPercent.Controls.Add(this.panel1);
            this.panelPercent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPercent.Location = new System.Drawing.Point(890, 0);
            this.panelPercent.Name = "panelPercent";
            this.panelPercent.Size = new System.Drawing.Size(322, 199);
            this.panelPercent.TabIndex = 14;
            // 
            // tableLayoutPercent
            // 
            this.tableLayoutPercent.ColumnCount = 2;
            this.tableLayoutPercent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.07542F));
            this.tableLayoutPercent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.92458F));
            this.tableLayoutPercent.Controls.Add(this.panelPercenBatterPC, 1, 0);
            this.tableLayoutPercent.Controls.Add(this.panelPercenBatt, 0, 0);
            this.tableLayoutPercent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPercent.Location = new System.Drawing.Point(0, 50);
            this.tableLayoutPercent.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPercent.Name = "tableLayoutPercent";
            this.tableLayoutPercent.RowCount = 1;
            this.tableLayoutPercent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPercent.Size = new System.Drawing.Size(322, 149);
            this.tableLayoutPercent.TabIndex = 17;
            // 
            // panelPercenBatterPC
            // 
            this.panelPercenBatterPC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.panelPercenBatterPC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPercenBatterPC.Location = new System.Drawing.Point(161, 0);
            this.panelPercenBatterPC.Margin = new System.Windows.Forms.Padding(0);
            this.panelPercenBatterPC.Name = "panelPercenBatterPC";
            this.panelPercenBatterPC.Size = new System.Drawing.Size(161, 149);
            this.panelPercenBatterPC.TabIndex = 1;
            // 
            // panelPercenBatt
            // 
            this.panelPercenBatt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.panelPercenBatt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPercenBatt.Location = new System.Drawing.Point(0, 0);
            this.panelPercenBatt.Margin = new System.Windows.Forms.Padding(0);
            this.panelPercenBatt.Name = "panelPercenBatt";
            this.panelPercenBatt.Size = new System.Drawing.Size(161, 149);
            this.panelPercenBatt.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.panel1.Controls.Add(this.LabelnameBatteryStatus);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(322, 50);
            this.panel1.TabIndex = 16;
            // 
            // LabelnameBatteryStatus
            // 
            this.LabelnameBatteryStatus.AutoSize = true;
            this.LabelnameBatteryStatus.Font = new System.Drawing.Font("Segoe UI", 11.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelnameBatteryStatus.ForeColor = System.Drawing.Color.White;
            this.LabelnameBatteryStatus.Location = new System.Drawing.Point(14, 10);
            this.LabelnameBatteryStatus.Name = "LabelnameBatteryStatus";
            this.LabelnameBatteryStatus.Size = new System.Drawing.Size(148, 28);
            this.LabelnameBatteryStatus.TabIndex = 5;
            this.LabelnameBatteryStatus.Text = "Battery Status";
            // 
            // tableLayoutPanelToggle
            // 
            this.tableLayoutPanelToggle.ColumnCount = 3;
            this.tableLayoutPanelToggle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanelToggle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanelToggle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanelToggle.Controls.Add(this.panelAntenna, 0, 0);
            this.tableLayoutPanelToggle.Controls.Add(this.panelShutdown, 2, 0);
            this.tableLayoutPanelToggle.Controls.Add(this.panelSwitch, 1, 0);
            this.tableLayoutPanelToggle.Dock = System.Windows.Forms.DockStyle.Right;
            this.tableLayoutPanelToggle.Location = new System.Drawing.Point(1212, 0);
            this.tableLayoutPanelToggle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanelToggle.Name = "tableLayoutPanelToggle";
            this.tableLayoutPanelToggle.RowCount = 1;
            this.tableLayoutPanelToggle.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelToggle.Size = new System.Drawing.Size(730, 199);
            this.tableLayoutPanelToggle.TabIndex = 13;
            // 
            // panelAntenna
            // 
            this.panelAntenna.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.panelAntenna.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAntenna.Location = new System.Drawing.Point(0, 0);
            this.panelAntenna.Margin = new System.Windows.Forms.Padding(0);
            this.panelAntenna.Name = "panelAntenna";
            this.panelAntenna.Size = new System.Drawing.Size(248, 199);
            this.panelAntenna.TabIndex = 2;
            // 
            // panelShutdown
            // 
            this.panelShutdown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.panelShutdown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelShutdown.Location = new System.Drawing.Point(488, 0);
            this.panelShutdown.Margin = new System.Windows.Forms.Padding(0);
            this.panelShutdown.Name = "panelShutdown";
            this.panelShutdown.Size = new System.Drawing.Size(242, 199);
            this.panelShutdown.TabIndex = 1;
            // 
            // panelSwitch
            // 
            this.panelSwitch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.panelSwitch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSwitch.Location = new System.Drawing.Point(248, 0);
            this.panelSwitch.Margin = new System.Windows.Forms.Padding(0);
            this.panelSwitch.Name = "panelSwitch";
            this.panelSwitch.Size = new System.Drawing.Size(240, 199);
            this.panelSwitch.TabIndex = 0;
            // 
            // tableLayoutCharger
            // 
            this.tableLayoutCharger.ColumnCount = 2;
            this.tableLayoutCharger.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutCharger.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutCharger.Controls.Add(this.panelAmp, 1, 0);
            this.tableLayoutCharger.Controls.Add(this.panelVolt, 0, 0);
            this.tableLayoutCharger.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutCharger.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutCharger.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutCharger.Name = "tableLayoutCharger";
            this.tableLayoutCharger.RowCount = 1;
            this.tableLayoutCharger.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutCharger.Size = new System.Drawing.Size(890, 199);
            this.tableLayoutCharger.TabIndex = 1;
            // 
            // panelAmp
            // 
            this.panelAmp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.panelAmp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAmp.Location = new System.Drawing.Point(445, 0);
            this.panelAmp.Margin = new System.Windows.Forms.Padding(0);
            this.panelAmp.Name = "panelAmp";
            this.panelAmp.Size = new System.Drawing.Size(445, 199);
            this.panelAmp.TabIndex = 1;
            // 
            // panelVolt
            // 
            this.panelVolt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.panelVolt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelVolt.Location = new System.Drawing.Point(0, 0);
            this.panelVolt.Margin = new System.Windows.Forms.Padding(0);
            this.panelVolt.Name = "panelVolt";
            this.panelVolt.Size = new System.Drawing.Size(445, 199);
            this.panelVolt.TabIndex = 0;
            // 
            // panelHead
            // 
            this.panelHead.BackColor = System.Drawing.Color.Transparent;
            this.panelHead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelHead.Controls.Add(this.panelName);
            this.panelHead.Controls.Add(this.panelClock);
            this.panelHead.Controls.Add(this.panelLogo);
            this.panelHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHead.Location = new System.Drawing.Point(0, 0);
            this.panelHead.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelHead.Name = "panelHead";
            this.panelHead.Size = new System.Drawing.Size(1942, 51);
            this.panelHead.TabIndex = 0;
            // 
            // panelName
            // 
            this.panelName.Controls.Add(this.brandname);
            this.panelName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelName.Location = new System.Drawing.Point(196, 0);
            this.panelName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelName.Name = "panelName";
            this.panelName.Size = new System.Drawing.Size(1175, 51);
            this.panelName.TabIndex = 2;
            // 
            // brandname
            // 
            this.brandname.AutoSize = true;
            this.brandname.Dock = System.Windows.Forms.DockStyle.Left;
            this.brandname.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.brandname.ForeColor = System.Drawing.Color.White;
            this.brandname.Location = new System.Drawing.Point(0, 0);
            this.brandname.Name = "brandname";
            this.brandname.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
            this.brandname.Size = new System.Drawing.Size(354, 43);
            this.brandname.TabIndex = 5;
            this.brandname.Text = "Jammer Remote Control";
            // 
            // panelClock
            // 
            this.panelClock.Controls.Add(this.labelDititalClock);
            this.panelClock.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelClock.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelClock.Location = new System.Drawing.Point(1371, 0);
            this.panelClock.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelClock.Name = "panelClock";
            this.panelClock.Padding = new System.Windows.Forms.Padding(5, 5, 11, 5);
            this.panelClock.Size = new System.Drawing.Size(571, 51);
            this.panelClock.TabIndex = 1;
            // 
            // labelDititalClock
            // 
            this.labelDititalClock.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDititalClock.AutoSize = true;
            this.labelDititalClock.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDititalClock.ForeColor = System.Drawing.Color.White;
            this.labelDititalClock.Location = new System.Drawing.Point(183, 9);
            this.labelDititalClock.Name = "labelDititalClock";
            this.labelDititalClock.Size = new System.Drawing.Size(124, 35);
            this.labelDititalClock.TabIndex = 2;
            this.labelDititalClock.Text = "02:30 PM";
            // 
            // panelLogo
            // 
            this.panelLogo.Controls.Add(this.pictureBoxLogo);
            this.panelLogo.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLogo.Location = new System.Drawing.Point(0, 0);
            this.panelLogo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Padding = new System.Windows.Forms.Padding(15, 5, 5, 5);
            this.panelLogo.Size = new System.Drawing.Size(196, 51);
            this.panelLogo.TabIndex = 0;
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.BackgroundImage = global::drc.Properties.Resources.drc_innovation;
            this.pictureBoxLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxLogo.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBoxLogo.Location = new System.Drawing.Point(15, 5);
            this.pictureBoxLogo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(168, 41);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxLogo.TabIndex = 2;
            this.pictureBoxLogo.TabStop = false;
            // 
            // timer_maindesktop
            // 
            this.timer_maindesktop.Enabled = true;
            this.timer_maindesktop.Interval = 1000;
            this.timer_maindesktop.Tick += new System.EventHandler(this.Timer_maindesktop_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1942, 1102);
            this.Controls.Add(this.panelBackground);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form2";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form2_Load);
            this.panelBackground.ResumeLayout(false);
            this.panelCenter.ResumeLayout(false);
            this.panel10Meter.ResumeLayout(false);
            this.tableLayoutPanel12Meter.ResumeLayout(false);
            this.panelBottom.ResumeLayout(false);
            this.panelPercent.ResumeLayout(false);
            this.tableLayoutPercent.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanelToggle.ResumeLayout(false);
            this.tableLayoutCharger.ResumeLayout(false);
            this.panelHead.ResumeLayout(false);
            this.panelName.ResumeLayout(false);
            this.panelName.PerformLayout();
            this.panelClock.ResumeLayout(false);
            this.panelClock.PerformLayout();
            this.panelLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelLogo;
        private System.Windows.Forms.Panel panelClock;
        private System.Windows.Forms.Panel panelName;
        private System.Windows.Forms.Panel panelHead;
        private System.Windows.Forms.Panel panelBackground;
        private System.Windows.Forms.Label labelDititalClock;
        private System.Windows.Forms.Panel panelCenter;
        private System.Windows.Forms.Panel panel10Meter;
        private System.Windows.Forms.Timer timer_maindesktop;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12Meter;
        private System.Windows.Forms.Panel PanelGauge12;
        private System.Windows.Forms.Panel PanelGauge11;
        private System.Windows.Forms.Panel PanelGauge10;
        private System.Windows.Forms.Panel PanelGauge9;
        private System.Windows.Forms.Panel PanelGauge8;
        private System.Windows.Forms.Panel PanelGauge7;
        private System.Windows.Forms.Panel PanelGauge6;
        private System.Windows.Forms.Panel PanelGauge5;
        private System.Windows.Forms.Panel PanelGauge4;
        private System.Windows.Forms.Panel PanelGauge1;
        private System.Windows.Forms.Panel PanelGauge2;
        private System.Windows.Forms.Panel PanelGauge3;
        private System.Windows.Forms.Panel PanelGauge14;
        private System.Windows.Forms.Panel PanelGauge13;
        private System.Windows.Forms.Panel PanelGauge15;
        private System.Windows.Forms.Label brandname;
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelToggle;
        private System.Windows.Forms.Panel panelAntenna;
        private System.Windows.Forms.Panel panelShutdown;
        private System.Windows.Forms.Panel panelSwitch;
        private System.Windows.Forms.TableLayoutPanel tableLayoutCharger;
        private System.Windows.Forms.Panel panelAmp;
        private System.Windows.Forms.Panel panelVolt;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.Panel panelPercent;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label LabelnameBatteryStatus;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPercent;
        private System.Windows.Forms.Panel panelPercenBatterPC;
        private System.Windows.Forms.Panel panelPercenBatt;
    }
}