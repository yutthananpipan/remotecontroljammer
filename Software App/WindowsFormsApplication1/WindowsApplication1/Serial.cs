﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;

namespace drc
{
    class Serial
    {

        private SerialPort _SerialcomPort;
        private String _SerialPortName = null;

        public Serial()
        {
            _SerialcomPort = new SerialPort();
            _SerialcomPort.DataReceived += new SerialDataReceivedEventHandler(SerialDataReceived);
        }

        ~Serial()
        {
            _SerialcomPort.Close();
        }

        public delegate void DataReceivedEventHandler(Object sender, RecieveDataEvent e);

        public event DataReceivedEventHandler DataReceivedEvent;

        public class RecieveDataEvent : EventArgs
        {
            internal byte[] _Buffer = new byte[20];
            internal int _NumberDataRecieve = 0;
            public byte[] Buffer
            {
                get
                {
                    return _Buffer;
                }
            }

            public int NumberDataRecieve
            {
                get
                {
                    return _NumberDataRecieve;
                }
            }
        }

        public String SerialPortName
        {
            get
            {
                return _SerialPortName;
            }
            set 
            {
                _SerialPortName = value;
            }
        }

        public bool OpenComPort(string comportName)
        {
            try
            {
                // Check old port
                if (_SerialcomPort.IsOpen)
                {
                    _SerialcomPort.Close();
                }
                _SerialPortName = comportName;
                // Allow the user to set the appropriate properties.
                _SerialcomPort.PortName = _SerialPortName;
                _SerialcomPort.BaudRate = 9600;
                _SerialcomPort.DataBits = 8;
                _SerialcomPort.StopBits = StopBits.One;
                _SerialcomPort.Parity = Parity.None;
                _SerialcomPort.ReadTimeout = 500;
                _SerialcomPort.WriteTimeout = 500;
                // Check new port
                _SerialcomPort.PortName = _SerialPortName;
                if (_SerialcomPort.IsOpen)
                {
                    _SerialcomPort.Close();
                }
                _SerialcomPort.Open();

                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public bool CheckConnectionComport()
        {

                if (_SerialcomPort.IsOpen)
                {
                    return true;
                } 
                else
                {
                    return false;
                }
        }

        public bool CloseComPort()
        {
            try
            {
                if (_SerialcomPort.IsOpen)
                    _SerialcomPort.Close();

                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public void SendData(byte[] data)
        {
            if (_SerialcomPort.IsOpen)
            {
                _SerialcomPort.Write(data, 0, data.Length);
            }
        }

        private void SerialDataReceived(Object sender, SerialDataReceivedEventArgs e)
        {
            int readByte = _SerialcomPort.BytesToRead;
            if (readByte == 10)
            {
                RecieveDataEvent fireArg = new RecieveDataEvent();
                fireArg._NumberDataRecieve = readByte;
                Array.Clear(fireArg._Buffer, 0, fireArg._Buffer.Length); 
          
                for (ushort i = 0; i < readByte; i++)
                {
                    fireArg._Buffer[i] = ((byte)_SerialcomPort.ReadByte());
                   
                }
                if (DataReceivedEvent != null)
                {
                    DataReceivedEvent(this, fireArg);
                }
            }
        }

        public void DiscardBuffer()
        {
            if (_SerialcomPort.IsOpen)
            {
                _SerialcomPort.DiscardInBuffer();
                _SerialcomPort.ReadExisting();
            } 
            else
            {

            }

        }
    }
}
