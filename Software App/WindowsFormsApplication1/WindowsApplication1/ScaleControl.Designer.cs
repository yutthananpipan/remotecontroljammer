﻿namespace drc
{
    partial class ScaleControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelVolt = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panelMeter = new System.Windows.Forms.Panel();
            this.labelUnit = new System.Windows.Forms.Label();
            this.labelDigital = new System.Windows.Forms.Label();
            this.meter1 = new NationalInstruments.UI.WindowsForms.Meter();
            this.panelVolt.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelMeter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meter1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelVolt
            // 
            this.panelVolt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.panelVolt.Controls.Add(this.panel3);
            this.panelVolt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelVolt.Location = new System.Drawing.Point(0, 0);
            this.panelVolt.Name = "panelVolt";
            this.panelVolt.Size = new System.Drawing.Size(446, 199);
            this.panelVolt.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panelMeter);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(446, 199);
            this.panel3.TabIndex = 7;
            // 
            // panelMeter
            // 
            this.panelMeter.BackColor = System.Drawing.Color.Transparent;
            this.panelMeter.BackgroundImage = global::drc.Properties.Resources.halfgauge_gray;
            this.panelMeter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelMeter.Controls.Add(this.labelUnit);
            this.panelMeter.Controls.Add(this.labelDigital);
            this.panelMeter.Controls.Add(this.meter1);
            this.panelMeter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMeter.Location = new System.Drawing.Point(0, 0);
            this.panelMeter.Name = "panelMeter";
            this.panelMeter.Padding = new System.Windows.Forms.Padding(50, 50, 50, 10);
            this.panelMeter.Size = new System.Drawing.Size(446, 199);
            this.panelMeter.TabIndex = 8;
            // 
            // labelUnit
            // 
            this.labelUnit.AutoSize = true;
            this.labelUnit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(223)))), ((int)(((byte)(224)))));
            this.labelUnit.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUnit.ForeColor = System.Drawing.Color.DimGray;
            this.labelUnit.Location = new System.Drawing.Point(208, 102);
            this.labelUnit.Name = "labelUnit";
            this.labelUnit.Size = new System.Drawing.Size(32, 35);
            this.labelUnit.TabIndex = 29;
            this.labelUnit.Text = "V";
            // 
            // labelDigital
            // 
            this.labelDigital.AutoSize = true;
            this.labelDigital.BackColor = System.Drawing.SystemColors.Control;
            this.labelDigital.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDigital.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelDigital.Location = new System.Drawing.Point(208, 160);
            this.labelDigital.Name = "labelDigital";
            this.labelDigital.Size = new System.Drawing.Size(37, 23);
            this.labelDigital.TabIndex = 28;
            this.labelDigital.Text = "000";
            // 
            // meter1
            // 
            this.meter1.DialColor = System.Drawing.Color.Transparent;
            this.meter1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meter1.Location = new System.Drawing.Point(50, 50);
            this.meter1.Name = "meter1";
            this.meter1.Range = new NationalInstruments.UI.Range(0D, 30D);
            this.meter1.Size = new System.Drawing.Size(346, 139);
            this.meter1.TabIndex = 26;
            // 
            // ScaleControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelVolt);
            this.Name = "ScaleControl";
            this.Size = new System.Drawing.Size(446, 199);
            this.Load += new System.EventHandler(this.Control1_Load);
            this.Resize += new System.EventHandler(this.Control1_Resize);
            this.panelVolt.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panelMeter.ResumeLayout(false);
            this.panelMeter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.meter1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelVolt;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panelMeter;
        private NationalInstruments.UI.WindowsForms.Meter meter1;
        private System.Windows.Forms.Label labelDigital;
        private System.Windows.Forms.Label labelUnit;
    }
}
