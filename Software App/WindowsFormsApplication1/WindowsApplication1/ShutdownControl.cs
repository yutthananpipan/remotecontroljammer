﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace drc
{
    public partial class ShutdownControl : UserControl
    {
        public enum PowerMode : int { Battery = 0, AC = 1, OFF = 2 };

        public ShutdownControl()
        {
            InitializeComponent();
        }

        public PictureBox getShutdown
        {
            get { return this.ShutdownIMG; }
        }

        public void Init()
        {
            //this.Shutdown.Location = new System.Drawing.Point((this.Shutdown.Parent.Width - this.Shutdown.Width) / 2, (this.Shutdown.Parent.Height - this.Shutdown.Height) / 2);
            this.labelname.Location = new System.Drawing.Point((this.labelname.Parent.Width - this.labelname.Width) / 2, (this.labelname.Parent.Height - this.labelname.Height) / 2);
        }

        private void ShutdownControl1_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            Init();
        }

        private void ShutdownControl1_Resize(object sender, EventArgs e)
        {
            Init();
        }

        public void ChangePowerMode(PowerMode PWMode)
        {
            Invoke((MethodInvoker)delegate
            {
                switch (PWMode){
                    case PowerMode.Battery:
                        this.ShutdownIMG.Image = Properties.Resources.btn_shutdown_orange;
                        this.charge_mode.Text = "Batt";
                        break;
                    case PowerMode.AC:
                        this.ShutdownIMG.Image = Properties.Resources.btn_shutdown_blue;
                        this.charge_mode.Text = "AC";
                        break;
                    case PowerMode.OFF:
                    default:
                        this.ShutdownIMG.Image = Properties.Resources.btn_shutdown;
                        this.charge_mode.Text = "";
                        break;

                }
                
            });
        }

        private void PanelSwitch_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
