﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using drc.Model;
using System.Drawing;

namespace drc
{
    public partial class MainForm : Form
    {
        private int handler_interval = 400;
        public enum DigitalColor { RedColor, BlueColor, GreenColor };
        public enum Type : int { Charger = 0, JammerModule = 1, Antenna = 2 };

        //delegate
        public delegate void TimeDelegate();
        public delegate void LEDDelegate(bool IsOn);
        public delegate void GaugeDelegate(int gauge, string temp, string volt, string amp);
        public delegate void MeterDelegate(int value);
        public delegate void BatteryModeDelegate(bool operate, bool ready, bool charge, bool nobatt);

        private List<GaugeControl> list_GaugeControl = new List<GaugeControl>();
        private List<JammerTask> listJammerTask = new List<JammerTask>();

        private List<Panel> listPanel = new List<Panel>();

        public List<GaugeControl> GetListGuageControl
        {
            get { return this.list_GaugeControl; }
        }

        public SwitchControl mainswitchControl;
        public ScaleControl voltControl;
        public ScaleControl ampControl;
        public ShutdownControl shutdownControl;
        public PercenBatt JammerBattery;
        public PercenBatt PCBattery;
        public SwitchControl antenaControl;

        public class RecieveDataEvent : EventArgs
        {
            internal byte[] _Buffer = new byte[20];
            internal int _NumberDataRecieve = 0;
            public byte[] Buffer
            {
                get
                {
                    return _Buffer;
                }
            }

            public int NumberDataRecieve
            {
                get
                {
                    return _NumberDataRecieve;
                }
            }
        }

        public MainForm()
        {
            InitializeComponent();
            AddPanel();

            AddPowerControl();
            LoadInitFile();
        }

        private void AddPanel()
        {
            listPanel.Add(PanelGauge1);
            listPanel.Add(PanelGauge2);
            listPanel.Add(PanelGauge3);
            listPanel.Add(PanelGauge4);
            listPanel.Add(PanelGauge5);
            listPanel.Add(PanelGauge6);
            listPanel.Add(PanelGauge7);
            listPanel.Add(PanelGauge8);
            listPanel.Add(PanelGauge9);
            listPanel.Add(PanelGauge10);
            listPanel.Add(PanelGauge11);
            listPanel.Add(PanelGauge12);
            listPanel.Add(PanelGauge13);
            listPanel.Add(PanelGauge14);
            listPanel.Add(PanelGauge15);
        }

        public void LoadInitFile()
        {
            try
            {
                List<int> listComport = new List<int>();

                using (StreamReader r = new StreamReader("data.json"))
                {
                    var str = r.ReadToEnd();
                    JObject json = JObject.Parse(str);

                    List<modules> mdObj = JsonConvert.DeserializeObject<List<modules>>(json["modules"].ToString());
                    foreach (modules obj in mdObj)
                    {
                        list_GaugeControl.Add(new GaugeControl(this, obj.id, obj.freq_name, obj.min_data, obj.max_data, obj.enable, obj.comport, Type.JammerModule));
                        if (listComport.Find(i => i == obj.comport) == 0)
                        {
                            listComport.Add(obj.comport);
                        }

                    }

                    AddGauge(list_GaugeControl);

                    brandname.Text = json["brandname"].ToString();
                    handler_interval = Convert.ToInt32(json["interval"]);

                    ScaleModel vtObj = JsonConvert.DeserializeObject<ScaleModel>(json["voltage"].ToString());
                    voltControl.InitData(vtObj);

                    ScaleModel crObj = JsonConvert.DeserializeObject<ScaleModel>(json["current"].ToString());
                    ampControl.InitData(crObj);

                    //Control Button
                    modules chargerObj = JsonConvert.DeserializeObject<modules>(json["charger"].ToString());
                    list_GaugeControl.Add(new GaugeControl(this, chargerObj.id, chargerObj.comport, Type.Charger));
                    if (listComport.Find(i => i == chargerObj.comport) == 0)
                    {
                        listComport.Add(chargerObj.comport);
                    }

                    List<modules> antenaObjs = JsonConvert.DeserializeObject<List<modules>>(json["antena"].ToString());
                    foreach (modules atnObj in antenaObjs)
                    {
                        list_GaugeControl.Add(new GaugeControl(this, atnObj.id, atnObj.comport, Type.Antenna));

                        if (listComport.Find(i => i == atnObj.comport) == 0)
                        {
                            listComport.Add(atnObj.comport);
                        }
                    }

                    CreateTask(listComport, handler_interval);

                }
            }
            catch (Exception e)
            {
                e.ToString();

            }
        }

        private void CreateTask(List<int> listComport, int handler_interval)
        {
            foreach (int cp in listComport)
            {
                List<GaugeControl> gauges = list_GaugeControl.FindAll(g => g.Comport == cp);
                listJammerTask.Add(new JammerTask(this, gauges, cp, this.components, handler_interval));
            }
        }

        private void AddGauge(List<GaugeControl> list_guage)
        {
            try
            {
                for (int i = 0; i < list_guage.Count; i++)
                {
                    listPanel[i].Controls.Add(list_guage[i]);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private bool CheckPortOpen()
        {
            foreach (JammerTask task in listJammerTask)
            {
                if (!task.SerialPort.IsOpen)
                {
                    return false;
                }
            }
            return true;
        }

        private void AddPowerControl()
        {
            // Init Charging
            voltControl = new ScaleControl();
            ampControl = new ScaleControl();
            voltControl.SetUnit("V");
            ampControl.SetUnit("A");

            // Init Battery
            JammerBattery = new PercenBatt(PercenBatt.BatteryType.Jammer);
            PCBattery = new PercenBatt(PercenBatt.BatteryType.PC);

            // Init Switch
            mainswitchControl = new SwitchControl();
            antenaControl = new SwitchControl();
            shutdownControl = new ShutdownControl();

            mainswitchControl.SetUnit("Remote SW");
            antenaControl.SetUnit("Antenna SW");

            panelSwitch.Controls.Add(mainswitchControl);
            panelVolt.Controls.Add(voltControl);
            panelAmp.Controls.Add(ampControl);

            panelShutdown.Controls.Add(shutdownControl);
            panelPercenBatt.Controls.Add(JammerBattery);
            panelPercenBatterPC.Controls.Add(PCBattery);
            panelAntenna.Controls.Add(antenaControl);

            this.mainswitchControl.getSwitch.Click += new System.EventHandler(this.MainSwitch_Click);
            this.shutdownControl.getShutdown.Click += new System.EventHandler(this.Shutdown_Click);
            this.antenaControl.getSwitch.Click += new System.EventHandler(this.AntenaSwitch_Click);

        }

        public void Message(string text)
        {
            WarningForm dialogfrm = new WarningForm();
            dialogfrm.getText.Text = text;
            dialogfrm.InitLocationLayout();
            dialogfrm.ShowDialog();
        }

        public void MessageDialog(string text)
        {
            WarningForm dialogfrm = new WarningForm();
            dialogfrm.FormBorderStyle = FormBorderStyle.None;
            dialogfrm.getText.Text = text;
            dialogfrm.InitLocationLayout();

            dialogfrm.Shown += new System.EventHandler(dialogfrm.dialogfrm_Shown);
            dialogfrm.ShowDialog();
        }

        public void Init()
        {
            this.labelDititalClock.Location = new System.Drawing.Point(this.labelDititalClock.Parent.Width - this.labelDititalClock.Width, (this.labelDititalClock.Parent.Height - this.labelDititalClock.Height) / 2);
            this.LabelnameBatteryStatus.Location = new System.Drawing.Point((this.LabelnameBatteryStatus.Parent.Width - this.LabelnameBatteryStatus.Width) / 2, (this.LabelnameBatteryStatus.Parent.Height - this.LabelnameBatteryStatus.Height) / 2);

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private bool CheckJammerOn()
        {
            List<GaugeControl> gauges = list_GaugeControl.FindAll(i => i.type == Type.JammerModule);
            foreach (GaugeControl g in gauges)
            {
                JammerTask task = g.JammerTask;
                if (task.Data_TX_Buffer[g.jammerID, GaugeControl.module_OnOff] == JammerTask.TxPowerOnJammer)
                {
                    return true;
                }
            }
            return false;
        }

        private void UpdateDigitalClock()
        {
            string format = "dddd, dd MMMM yyyy HH:mm:ss";
            DateTime now = DateTime.Now;
            this.labelDititalClock.Text = now.ToString(format);
            Init();
        }

        public void ResetUI()
        {
            try
            {
                foreach (GaugeControl usercontrol in list_GaugeControl)
                {
                    usercontrol.getStatusOn = false;

                    usercontrol.GaugeDataChange(0, "0", "0", "0");
                    usercontrol.SwitchChange();

                }

                voltControl.MeterDataChange(0);
                ampControl.MeterDataChange(0);
                //battmodeControl.LEDDataChange(false, false, false, false);
            }
            catch (Exception e)
            {
                e.ToString();
            }

        }

        private void Shutdown()
        {
            string filename = string.Empty;
            string arguments = string.Empty;
            filename = "shutdown.exe";
            arguments = "-s";
            ProcessStartInfo startinfo = new ProcessStartInfo(filename, arguments);
            Process.Start(startinfo);
            this.Close();
        }

        private void AntenaSwitch_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.mainswitchControl.switch_status == true)
                {
                    if (!this.antenaControl.switch_status == true)
                    {
                        this.antenaControl.switch_status = true;

                    }
                    else
                    {
                        if (CheckJammerOn())
                        {
                            Message("ไม่สามารถพับเสาได้ เนื่องจากมีแจมเมอร์กำลังใช้งาน");
                            return;
                        }

                        this.antenaControl.switch_status = false;
                    }

                    this.antenaControl.SwitchcOn(this.antenaControl.switch_status);

                    List<GaugeControl> gauges = list_GaugeControl.FindAll(i => i.type == Type.Antenna);
                    foreach (GaugeControl g in gauges)
                    {
                        JammerTask task = g.JammerTask;
                        if (task.SerialPort.IsOpen)
                        {
                            Thread thread = new Thread(() => task.ControlAntena(g.jammerID, this.antenaControl.switch_status));
                            thread.Start();
                            thread.Join();

                        }
                        else
                        {
                            Message("ไม่พบการเชื่อมต่อ Comport");

                            this.antenaControl.switch_status = false;
                            this.antenaControl.SwitchcOn(this.antenaControl.switch_status);

                            task.ControlAntena(g.jammerID, this.antenaControl.switch_status);
                            task.Start();

                            return;
                        }

                    }

                    MessageDialog((this.antenaControl.switch_status) ? "เสากำลังยก" : "เสากำลังเก็บ");
                }
                else
                {
                    Message("กรุณาเปิดเมนสวิตซ์");
                }

            }
            catch (Exception)
            {

            }
        }

        private void MainSwitch_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this.mainswitchControl.switch_status)
                {
                    try
                    {
                        this.mainswitchControl.switch_status = true;
                        this.mainswitchControl.SwitchcOn(this.mainswitchControl.switch_status);

                        foreach (JammerTask task in listJammerTask)
                        {
                            task.OpenSerialPort();
                        }

                        if (CheckPortOpen() == true)
                        {
                            foreach (GaugeControl usercontrol in list_GaugeControl)
                            {
                                usercontrol.SwitchChange();
                            }

                            foreach (JammerTask task in listJammerTask)
                            {
                                task.Start();
                            }
                        }
                        else
                        {
                            this.mainswitchControl.switch_status = false;
                            this.mainswitchControl.SwitchcOn(this.mainswitchControl.switch_status);

                            Message("ไม่พบการเชื่อมต่อ Comport");
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                    }
                }
                else
                {

                    this.mainswitchControl.switch_status = false;
                    this.mainswitchControl.SwitchcOn(this.mainswitchControl.switch_status);

                    foreach (JammerTask task in listJammerTask)
                    {
                        task.Pause();
                        task.ShutdownSystem();

                    }

                    foreach (GaugeControl usercontrol in list_GaugeControl)
                    {
                        usercontrol.SwitchChange();
                    }

                    foreach (JammerTask task in listJammerTask)
                    {
                        task.CloseSerialPort();
                    }

                }
            }
            catch (Exception ex1)
            {
                ex1.ToString();

            }

        }

        private void Shutdown_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    DialogResult dr = new DialogResult();
                    DialogForm dialogfrm = new DialogForm();
                    dr = dialogfrm.ShowDialog();
                    if (dr == DialogResult.OK)
                    {
                        this.mainswitchControl.switch_status = false;
                        foreach (JammerTask task in listJammerTask)
                        {
                            task.Pause();

                            task.ShutdownSystem();
                        }

                        new Thread(Shutdown).Start();
                        Application.ExitThread();
                        Application.Exit();
                    }
                }
                catch
                {
                    this.mainswitchControl.switch_status = false;
                }

            }
            catch (Exception)
            {

            }
            finally
            {
                //timer_Stop();
            }
        }

        private void Form2_ResizeEnd(object sender, EventArgs e)
        {
            Init();
        }

        private void Timer_maindesktop_Tick(object sender, EventArgs e)
        {
            Invoke(new TimeDelegate(UpdateDigitalClock), new object[] { });

            this.Invoke((MethodInvoker)delegate
            {
                // Get Percent Battery on pc
                PowerStatus p = SystemInformation.PowerStatus;
                int pb = (int)(p.BatteryLifePercent * 100);
                this.PCBattery.OnBatteryValueChange(pb);

            });

        }

    }
}
