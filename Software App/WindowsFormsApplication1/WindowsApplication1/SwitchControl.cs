﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace drc
{
    public partial class SwitchControl : UserControl
    {
        public bool switch_status = false;

        public SwitchControl()
        {
            InitializeComponent();
        }

        public PictureBox getSwitch
        {
            get { return pictureBoxSwitch; }
        }

        public void SetUnit(string unit)
        {
            labelname.Text = unit;
        }

        public void SwitchcOn(bool isOn)
        {
            try
            {
                this.pictureBoxSwitch.Image = (isOn) ? global::drc.Properties.Resources.switch_off : global::drc.Properties.Resources.switch_on;
                this.led.Image = (isOn) ? global::drc.Properties.Resources.led_red : global::drc.Properties.Resources.led_gray;

            }
            catch (Exception)
            {
            }
        }

        public void Init()
        {
            this.pictureBoxSwitch.Location = new System.Drawing.Point((this.pictureBoxSwitch.Parent.Width - this.pictureBoxSwitch.Width) / 2, (this.pictureBoxSwitch.Parent.Height - this.pictureBoxSwitch.Height) / 2);
            this.labelname.Location = new System.Drawing.Point((this.labelname.Parent.Width - this.labelname.Width) / 2, (this.labelname.Parent.Height - this.labelname.Height) / 2);

        }

        private void SwitchControl1_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            Init();
        }

        private void SwitchControl1_Resize(object sender, EventArgs e)
        {
            Init();
        }

        private void PictureBoxSwitch_Click(object sender, EventArgs e)
        {

        }

    }
}
