﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace drc
{
    public partial class WarningForm : Form
    {
        public WarningForm()
        {
            InitializeComponent();

            InitLocationLayout();
        }

        public Label getText
        {
            get { return text; }
            set { this.text = value; }
        }

        public void InitLocationLayout()
        {
            this.text.Location = new System.Drawing.Point((int)(this.text.Parent.Width / 2) - (int)(this.text.Width / 2), (this.text.Parent.Height / 2) - (this.text.Height / 2));

        }

        public void dialogfrm_Shown(object sender, EventArgs e)
        {
            Timer timer1 = new Timer();
            timer1.Interval = 3000;
            timer1.Tick += new System.EventHandler(this.timer1_Tick);
            timer1.Start();
        }

        private void timer1_Tick(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
