﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using drc.Model;

namespace drc
{
    public partial class ScaleControl : UserControl
    {
        NationalInstruments.UI.ScaleRangeFill scaleRangeFill1 = new NationalInstruments.UI.ScaleRangeFill();
        NationalInstruments.UI.ScaleRangeFill scaleRangeFill2 = new NationalInstruments.UI.ScaleRangeFill();
        NationalInstruments.UI.ScaleRangeFill scaleRangeFill3 = new NationalInstruments.UI.ScaleRangeFill();

        public ScaleControl()
        {
            InitializeComponent();
        }

        public NationalInstruments.UI.WindowsForms.Meter getMeter
        {
            get { return this.meter1; }
            set { this.meter1 = value; }
        } 

        public void InitData(Object vtObj)
        {
            ScaleModel obj = (ScaleModel)vtObj;
            this.meter1.Range = new NationalInstruments.UI.Range(Convert.ToDouble(obj.min_data), Convert.ToDouble(obj.max_data));

            try
            {
                scaleRangeFill1.Range = new NationalInstruments.UI.Range(Convert.ToDouble(obj.danger_min_range), Convert.ToDouble(obj.danger_max_range));
                scaleRangeFill1.Style = NationalInstruments.UI.ScaleRangeFillStyle.CreateSolidStyle(System.Drawing.Color.Red);
                scaleRangeFill2.Range = new NationalInstruments.UI.Range(Convert.ToDouble(obj.warning_min_range), Convert.ToDouble(obj.warning_max_range));
                scaleRangeFill2.Style = NationalInstruments.UI.ScaleRangeFillStyle.CreateSolidStyle(System.Drawing.Color.Yellow);
                scaleRangeFill3.Range = new NationalInstruments.UI.Range(Convert.ToDouble(obj.normal_min_range), Convert.ToDouble(obj.normal_max_range));
                scaleRangeFill3.Style = NationalInstruments.UI.ScaleRangeFillStyle.CreateSolidStyle(System.Drawing.Color.Lime);

                this.meter1.RangeFills.AddRange(new NationalInstruments.UI.ScaleRangeFill[] {
            scaleRangeFill1,
            scaleRangeFill2,
            scaleRangeFill3});
            }
            catch(Exception e)
            {
                e.ToString();
            }

        }

        private void Control1_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            Init();
        }

        private void UpdateUI(int value)
        {
            try
            {
                this.meter1.Value = Convert.ToDouble(value);
                this.labelDigital.Text = Convert.ToString(value);
                
            }
            catch (Exception) { }
        }

        public void SetUnit(string unit)
        {
            labelUnit.Text = unit;
        }

        public void Init()
        {
            //this.labelname.Location = new System.Drawing.Point(this.panelname.Width / 2 - this.labelname.Width / 2, this.panelname.Height / 2 - this.labelname.Height / 2);

            //this.panelMeter.Location = new System.Drawing.Point((this.panelMeter.Parent.Width - this.panelMeter.Width) / 2, (this.panelMeter.Parent.Height - this.panelMeter.Height) / 2);
            this.labelDigital.Location = new System.Drawing.Point((this.labelDigital.Parent.Width - this.labelDigital.Width) / 2, (this.labelDigital.Parent.Height - this.labelDigital.Height) - 15);
            //this.labelUnit.Location = new System.Drawing.Point((this.labelUnit.Parent.Width - this.labelUnit.Width) / 2, (this.labelUnit.Parent.Height - this.labelUnit.Height) / 2 + 15);
        }

        public void MeterDataChange(int value)
        {
            Thread thread = new Thread(() => UpdateUI(value));
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            while (thread.IsAlive)
            {
                Application.DoEvents();
            }

            try
            {
                this.labelDigital.Text = Convert.ToString(value);
                Init();
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }

        private void Control1_Resize(object sender, EventArgs e)
        {
            Init();
        }
    }
}
