﻿namespace drc
{
    partial class GaugeControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GaugeControl));
            this.panelFreqName1 = new System.Windows.Forms.Panel();
            this.labelname = new System.Windows.Forms.Label();
            this.panelMeter1 = new System.Windows.Forms.Panel();
            this.panelDisplay = new System.Windows.Forms.Panel();
            this.panelRadialGauge = new System.Windows.Forms.Panel();
            this.panelLed = new System.Windows.Forms.Panel();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.panelDigital = new System.Windows.Forms.Panel();
            this.panelBTN = new System.Windows.Forms.Panel();
            this.tableLayoutPanelButton = new System.Windows.Forms.TableLayoutPanel();
            this.panelHiLow = new System.Windows.Forms.Panel();
            this.HiLowBtn = new EnhancedGlassButton.GlassButton();
            this.panelOnOff = new System.Windows.Forms.Panel();
            this.OnOffBtn = new EnhancedGlassButton.GlassButton();
            this.tableLayoutPanelDigital = new System.Windows.Forms.TableLayoutPanel();
            this.panelamp = new System.Windows.Forms.Panel();
            this.UnitAmp = new System.Windows.Forms.Label();
            this.labelAmp1 = new System.Windows.Forms.Label();
            this.panelvolt = new System.Windows.Forms.Panel();
            this.UnitVolt = new System.Windows.Forms.Label();
            this.labelVolt1 = new System.Windows.Forms.Label();
            this.paneltemp = new System.Windows.Forms.Panel();
            this.UnitTemp = new System.Windows.Forms.Label();
            this.labelTemp1 = new System.Windows.Forms.Label();
            this.panelWatt = new System.Windows.Forms.Panel();
            this.UnitWatt = new System.Windows.Forms.Label();
            this.labelgauge = new System.Windows.Forms.Label();
            this.panellight = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureARU = new System.Windows.Forms.PictureBox();
            this.led_TX = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureARD = new System.Windows.Forms.PictureBox();
            this.led_RX = new System.Windows.Forms.PictureBox();
            this.panelMeter = new System.Windows.Forms.Panel();
            this.panelGauge = new System.Windows.Forms.Panel();
            this.WattMeter = new NationalInstruments.UI.WindowsForms.Gauge();
            this.labelwatt = new System.Windows.Forms.Label();
            this.panelControl = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panelFreqName1.SuspendLayout();
            this.panelMeter1.SuspendLayout();
            this.panelDisplay.SuspendLayout();
            this.panelRadialGauge.SuspendLayout();
            this.panelLed.SuspendLayout();
            this.panelMenu.SuspendLayout();
            this.panelDigital.SuspendLayout();
            this.panelBTN.SuspendLayout();
            this.tableLayoutPanelButton.SuspendLayout();
            this.panelHiLow.SuspendLayout();
            this.panelOnOff.SuspendLayout();
            this.tableLayoutPanelDigital.SuspendLayout();
            this.panelamp.SuspendLayout();
            this.panelvolt.SuspendLayout();
            this.paneltemp.SuspendLayout();
            this.panelWatt.SuspendLayout();
            this.panellight.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureARU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.led_TX)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureARD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.led_RX)).BeginInit();
            this.panelMeter.SuspendLayout();
            this.panelGauge.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WattMeter)).BeginInit();
            this.WattMeter.SuspendLayout();
            this.panelControl.SuspendLayout();
            this.panel12.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelFreqName1
            // 
            this.panelFreqName1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(119)))), ((int)(((byte)(130)))));
            this.panelFreqName1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelFreqName1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelFreqName1.Controls.Add(this.labelname);
            this.panelFreqName1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFreqName1.Location = new System.Drawing.Point(0, 0);
            this.panelFreqName1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelFreqName1.Name = "panelFreqName1";
            this.panelFreqName1.Size = new System.Drawing.Size(523, 28);
            this.panelFreqName1.TabIndex = 0;
            // 
            // labelname
            // 
            this.labelname.AutoSize = true;
            this.labelname.Font = new System.Drawing.Font("Segoe UI", 9.8F, System.Drawing.FontStyle.Bold);
            this.labelname.ForeColor = System.Drawing.Color.DarkGray;
            this.labelname.Location = new System.Drawing.Point(127, 2);
            this.labelname.Name = "labelname";
            this.labelname.Size = new System.Drawing.Size(0, 23);
            this.labelname.TabIndex = 5;
            // 
            // panelMeter1
            // 
            this.panelMeter1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.panelMeter1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelMeter1.Controls.Add(this.panelDisplay);
            this.panelMeter1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMeter1.Location = new System.Drawing.Point(0, 28);
            this.panelMeter1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelMeter1.Name = "panelMeter1";
            this.panelMeter1.Size = new System.Drawing.Size(523, 365);
            this.panelMeter1.TabIndex = 3;
            // 
            // panelDisplay
            // 
            this.panelDisplay.Controls.Add(this.panelRadialGauge);
            this.panelDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDisplay.Location = new System.Drawing.Point(0, 0);
            this.panelDisplay.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelDisplay.Name = "panelDisplay";
            this.panelDisplay.Size = new System.Drawing.Size(523, 365);
            this.panelDisplay.TabIndex = 4;
            // 
            // panelRadialGauge
            // 
            this.panelRadialGauge.BackColor = System.Drawing.Color.Transparent;
            this.panelRadialGauge.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelRadialGauge.Controls.Add(this.panelLed);
            this.panelRadialGauge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRadialGauge.Location = new System.Drawing.Point(0, 0);
            this.panelRadialGauge.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelRadialGauge.Name = "panelRadialGauge";
            this.panelRadialGauge.Size = new System.Drawing.Size(523, 365);
            this.panelRadialGauge.TabIndex = 5;
            // 
            // panelLed
            // 
            this.panelLed.Controls.Add(this.panelMenu);
            this.panelLed.Controls.Add(this.panelMeter);
            this.panelLed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLed.Location = new System.Drawing.Point(0, 0);
            this.panelLed.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelLed.Name = "panelLed";
            this.panelLed.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelLed.Size = new System.Drawing.Size(523, 365);
            this.panelLed.TabIndex = 28;
            // 
            // panelMenu
            // 
            this.panelMenu.Controls.Add(this.panelDigital);
            this.panelMenu.Controls.Add(this.panellight);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMenu.Location = new System.Drawing.Point(203, 2);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(317, 361);
            this.panelMenu.TabIndex = 14;
            // 
            // panelDigital
            // 
            this.panelDigital.Controls.Add(this.panelBTN);
            this.panelDigital.Controls.Add(this.tableLayoutPanelDigital);
            this.panelDigital.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDigital.Location = new System.Drawing.Point(0, 24);
            this.panelDigital.Name = "panelDigital";
            this.panelDigital.Padding = new System.Windows.Forms.Padding(3);
            this.panelDigital.Size = new System.Drawing.Size(317, 337);
            this.panelDigital.TabIndex = 29;
            // 
            // panelBTN
            // 
            this.panelBTN.Controls.Add(this.tableLayoutPanelButton);
            this.panelBTN.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelBTN.Location = new System.Drawing.Point(3, 103);
            this.panelBTN.Name = "panelBTN";
            this.panelBTN.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.panelBTN.Size = new System.Drawing.Size(311, 78);
            this.panelBTN.TabIndex = 38;
            // 
            // tableLayoutPanelButton
            // 
            this.tableLayoutPanelButton.ColumnCount = 2;
            this.tableLayoutPanelButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelButton.Controls.Add(this.panelHiLow, 0, 0);
            this.tableLayoutPanelButton.Controls.Add(this.panelOnOff, 1, 0);
            this.tableLayoutPanelButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanelButton.Location = new System.Drawing.Point(0, 3);
            this.tableLayoutPanelButton.Name = "tableLayoutPanelButton";
            this.tableLayoutPanelButton.RowCount = 1;
            this.tableLayoutPanelButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanelButton.Size = new System.Drawing.Size(311, 54);
            this.tableLayoutPanelButton.TabIndex = 39;
            // 
            // panelHiLow
            // 
            this.panelHiLow.Controls.Add(this.HiLowBtn);
            this.panelHiLow.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelHiLow.Location = new System.Drawing.Point(25, 2);
            this.panelHiLow.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelHiLow.Name = "panelHiLow";
            this.panelHiLow.Padding = new System.Windows.Forms.Padding(3);
            this.panelHiLow.Size = new System.Drawing.Size(127, 50);
            this.panelHiLow.TabIndex = 30;
            // 
            // HiLowBtn
            // 
            this.HiLowBtn.AlternativeFocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))));
            this.HiLowBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.HiLowBtn.Dock = System.Windows.Forms.DockStyle.Right;
            this.HiLowBtn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HiLowBtn.ForeColor = System.Drawing.Color.DimGray;
            this.HiLowBtn.GlowColor = System.Drawing.Color.Transparent;
            this.HiLowBtn.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(223)))), ((int)(((byte)(224)))));
            this.HiLowBtn.Location = new System.Drawing.Point(44, 3);
            this.HiLowBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.HiLowBtn.Name = "HiLowBtn";
            this.HiLowBtn.Size = new System.Drawing.Size(80, 44);
            this.HiLowBtn.TabIndex = 0;
            this.HiLowBtn.Text = "LOW";
            this.HiLowBtn.Click += new System.EventHandler(this.HiLowBtn_Click);
            // 
            // panelOnOff
            // 
            this.panelOnOff.Controls.Add(this.OnOffBtn);
            this.panelOnOff.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelOnOff.Location = new System.Drawing.Point(158, 2);
            this.panelOnOff.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelOnOff.Name = "panelOnOff";
            this.panelOnOff.Padding = new System.Windows.Forms.Padding(3);
            this.panelOnOff.Size = new System.Drawing.Size(130, 50);
            this.panelOnOff.TabIndex = 29;
            // 
            // OnOffBtn
            // 
            this.OnOffBtn.AlternativeFocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))));
            this.OnOffBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.OnOffBtn.Dock = System.Windows.Forms.DockStyle.Left;
            this.OnOffBtn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OnOffBtn.ForeColor = System.Drawing.Color.DimGray;
            this.OnOffBtn.GlowColor = System.Drawing.Color.Transparent;
            this.OnOffBtn.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(223)))), ((int)(((byte)(224)))));
            this.OnOffBtn.Location = new System.Drawing.Point(3, 3);
            this.OnOffBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.OnOffBtn.Name = "OnOffBtn";
            this.OnOffBtn.Size = new System.Drawing.Size(80, 44);
            this.OnOffBtn.TabIndex = 1;
            this.OnOffBtn.Text = "OFF";
            this.OnOffBtn.Click += new System.EventHandler(this.OnOffBtn_Click);
            // 
            // tableLayoutPanelDigital
            // 
            this.tableLayoutPanelDigital.ColumnCount = 2;
            this.tableLayoutPanelDigital.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelDigital.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelDigital.Controls.Add(this.panelamp, 0, 1);
            this.tableLayoutPanelDigital.Controls.Add(this.panelvolt, 1, 1);
            this.tableLayoutPanelDigital.Controls.Add(this.paneltemp, 0, 0);
            this.tableLayoutPanelDigital.Controls.Add(this.panelWatt, 1, 0);
            this.tableLayoutPanelDigital.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanelDigital.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelDigital.Name = "tableLayoutPanelDigital";
            this.tableLayoutPanelDigital.RowCount = 2;
            this.tableLayoutPanelDigital.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelDigital.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelDigital.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelDigital.Size = new System.Drawing.Size(311, 100);
            this.tableLayoutPanelDigital.TabIndex = 37;
            // 
            // panelamp
            // 
            this.panelamp.BackColor = System.Drawing.Color.Gainsboro;
            this.panelamp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelamp.Controls.Add(this.UnitAmp);
            this.panelamp.Controls.Add(this.labelAmp1);
            this.panelamp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelamp.Location = new System.Drawing.Point(3, 52);
            this.panelamp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelamp.Name = "panelamp";
            this.panelamp.Padding = new System.Windows.Forms.Padding(2);
            this.panelamp.Size = new System.Drawing.Size(149, 46);
            this.panelamp.TabIndex = 3;
            // 
            // UnitAmp
            // 
            this.UnitAmp.AutoSize = true;
            this.UnitAmp.Dock = System.Windows.Forms.DockStyle.Right;
            this.UnitAmp.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold);
            this.UnitAmp.ForeColor = System.Drawing.Color.DimGray;
            this.UnitAmp.Location = new System.Drawing.Point(122, 2);
            this.UnitAmp.Name = "UnitAmp";
            this.UnitAmp.Size = new System.Drawing.Size(25, 25);
            this.UnitAmp.TabIndex = 4;
            this.UnitAmp.Text = "A";
            // 
            // labelAmp1
            // 
            this.labelAmp1.AutoSize = true;
            this.labelAmp1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelAmp1.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.labelAmp1.ForeColor = System.Drawing.Color.DimGray;
            this.labelAmp1.Location = new System.Drawing.Point(2, 2);
            this.labelAmp1.Name = "labelAmp1";
            this.labelAmp1.Size = new System.Drawing.Size(22, 25);
            this.labelAmp1.TabIndex = 3;
            this.labelAmp1.Text = "0";
            // 
            // panelvolt
            // 
            this.panelvolt.BackColor = System.Drawing.Color.Gainsboro;
            this.panelvolt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelvolt.Controls.Add(this.UnitVolt);
            this.panelvolt.Controls.Add(this.labelVolt1);
            this.panelvolt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelvolt.Location = new System.Drawing.Point(158, 52);
            this.panelvolt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelvolt.Name = "panelvolt";
            this.panelvolt.Padding = new System.Windows.Forms.Padding(2);
            this.panelvolt.Size = new System.Drawing.Size(150, 46);
            this.panelvolt.TabIndex = 2;
            // 
            // UnitVolt
            // 
            this.UnitVolt.AutoSize = true;
            this.UnitVolt.Dock = System.Windows.Forms.DockStyle.Right;
            this.UnitVolt.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold);
            this.UnitVolt.ForeColor = System.Drawing.Color.DimGray;
            this.UnitVolt.Location = new System.Drawing.Point(123, 2);
            this.UnitVolt.Name = "UnitVolt";
            this.UnitVolt.Size = new System.Drawing.Size(25, 25);
            this.UnitVolt.TabIndex = 3;
            this.UnitVolt.Text = "V";
            // 
            // labelVolt1
            // 
            this.labelVolt1.AutoSize = true;
            this.labelVolt1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelVolt1.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.labelVolt1.ForeColor = System.Drawing.Color.DimGray;
            this.labelVolt1.Location = new System.Drawing.Point(2, 2);
            this.labelVolt1.Name = "labelVolt1";
            this.labelVolt1.Size = new System.Drawing.Size(22, 25);
            this.labelVolt1.TabIndex = 2;
            this.labelVolt1.Text = "0";
            // 
            // paneltemp
            // 
            this.paneltemp.BackColor = System.Drawing.Color.Gainsboro;
            this.paneltemp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.paneltemp.Controls.Add(this.UnitTemp);
            this.paneltemp.Controls.Add(this.labelTemp1);
            this.paneltemp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.paneltemp.Location = new System.Drawing.Point(3, 2);
            this.paneltemp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.paneltemp.Name = "paneltemp";
            this.paneltemp.Padding = new System.Windows.Forms.Padding(2);
            this.paneltemp.Size = new System.Drawing.Size(149, 46);
            this.paneltemp.TabIndex = 1;
            // 
            // UnitTemp
            // 
            this.UnitTemp.AutoSize = true;
            this.UnitTemp.Dock = System.Windows.Forms.DockStyle.Right;
            this.UnitTemp.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold);
            this.UnitTemp.ForeColor = System.Drawing.Color.DimGray;
            this.UnitTemp.Location = new System.Drawing.Point(116, 2);
            this.UnitTemp.Name = "UnitTemp";
            this.UnitTemp.Size = new System.Drawing.Size(31, 25);
            this.UnitTemp.TabIndex = 2;
            this.UnitTemp.Text = "°C";
            // 
            // labelTemp1
            // 
            this.labelTemp1.AutoSize = true;
            this.labelTemp1.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelTemp1.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.labelTemp1.ForeColor = System.Drawing.Color.DimGray;
            this.labelTemp1.Location = new System.Drawing.Point(2, 2);
            this.labelTemp1.Name = "labelTemp1";
            this.labelTemp1.Size = new System.Drawing.Size(22, 25);
            this.labelTemp1.TabIndex = 1;
            this.labelTemp1.Text = "0";
            // 
            // panelWatt
            // 
            this.panelWatt.BackColor = System.Drawing.Color.Gainsboro;
            this.panelWatt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelWatt.Controls.Add(this.UnitWatt);
            this.panelWatt.Controls.Add(this.labelgauge);
            this.panelWatt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWatt.Location = new System.Drawing.Point(158, 2);
            this.panelWatt.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelWatt.Name = "panelWatt";
            this.panelWatt.Padding = new System.Windows.Forms.Padding(2);
            this.panelWatt.Size = new System.Drawing.Size(150, 46);
            this.panelWatt.TabIndex = 5;
            // 
            // UnitWatt
            // 
            this.UnitWatt.AutoSize = true;
            this.UnitWatt.Dock = System.Windows.Forms.DockStyle.Right;
            this.UnitWatt.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold);
            this.UnitWatt.ForeColor = System.Drawing.Color.DimGray;
            this.UnitWatt.Location = new System.Drawing.Point(117, 2);
            this.UnitWatt.Name = "UnitWatt";
            this.UnitWatt.Size = new System.Drawing.Size(31, 25);
            this.UnitWatt.TabIndex = 2;
            this.UnitWatt.Text = "W";
            // 
            // labelgauge
            // 
            this.labelgauge.AutoSize = true;
            this.labelgauge.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelgauge.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.labelgauge.ForeColor = System.Drawing.Color.DimGray;
            this.labelgauge.Location = new System.Drawing.Point(2, 2);
            this.labelgauge.Name = "labelgauge";
            this.labelgauge.Size = new System.Drawing.Size(22, 25);
            this.labelgauge.TabIndex = 1;
            this.labelgauge.Text = "0";
            // 
            // panellight
            // 
            this.panellight.Controls.Add(this.panel2);
            this.panellight.Controls.Add(this.panel1);
            this.panellight.Dock = System.Windows.Forms.DockStyle.Top;
            this.panellight.Location = new System.Drawing.Point(0, 0);
            this.panellight.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panellight.Name = "panellight";
            this.panellight.Padding = new System.Windows.Forms.Padding(3);
            this.panellight.Size = new System.Drawing.Size(317, 24);
            this.panellight.TabIndex = 28;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pictureARU);
            this.panel2.Controls.Add(this.led_TX);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(198, 3);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.panel2.Size = new System.Drawing.Size(69, 18);
            this.panel2.TabIndex = 8;
            // 
            // pictureARU
            // 
            this.pictureARU.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureARU.Image = global::drc.Properties.Resources.arrow_up_gray;
            this.pictureARU.Location = new System.Drawing.Point(24, 0);
            this.pictureARU.Margin = new System.Windows.Forms.Padding(3, 2, 5, 2);
            this.pictureARU.Name = "pictureARU";
            this.pictureARU.Padding = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.pictureARU.Size = new System.Drawing.Size(20, 18);
            this.pictureARU.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureARU.TabIndex = 8;
            this.pictureARU.TabStop = false;
            // 
            // led_TX
            // 
            this.led_TX.Dock = System.Windows.Forms.DockStyle.Right;
            this.led_TX.Image = global::drc.Properties.Resources.led_gray;
            this.led_TX.Location = new System.Drawing.Point(44, 0);
            this.led_TX.Margin = new System.Windows.Forms.Padding(3, 2, 5, 2);
            this.led_TX.Name = "led_TX";
            this.led_TX.Padding = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.led_TX.Size = new System.Drawing.Size(20, 18);
            this.led_TX.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.led_TX.TabIndex = 7;
            this.led_TX.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureARD);
            this.panel1.Controls.Add(this.led_RX);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(267, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(47, 18);
            this.panel1.TabIndex = 7;
            // 
            // pictureARD
            // 
            this.pictureARD.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureARD.Image = global::drc.Properties.Resources.arrow_down_gray;
            this.pictureARD.Location = new System.Drawing.Point(7, 0);
            this.pictureARD.Margin = new System.Windows.Forms.Padding(3, 2, 5, 2);
            this.pictureARD.Name = "pictureARD";
            this.pictureARD.Padding = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.pictureARD.Size = new System.Drawing.Size(20, 18);
            this.pictureARD.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureARD.TabIndex = 9;
            this.pictureARD.TabStop = false;
            // 
            // led_RX
            // 
            this.led_RX.Dock = System.Windows.Forms.DockStyle.Right;
            this.led_RX.Image = global::drc.Properties.Resources.led_gray;
            this.led_RX.Location = new System.Drawing.Point(27, 0);
            this.led_RX.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.led_RX.Name = "led_RX";
            this.led_RX.Size = new System.Drawing.Size(20, 18);
            this.led_RX.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.led_RX.TabIndex = 5;
            this.led_RX.TabStop = false;
            // 
            // panelMeter
            // 
            this.panelMeter.Controls.Add(this.panelGauge);
            this.panelMeter.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMeter.Location = new System.Drawing.Point(3, 2);
            this.panelMeter.Name = "panelMeter";
            this.panelMeter.Padding = new System.Windows.Forms.Padding(3);
            this.panelMeter.Size = new System.Drawing.Size(200, 361);
            this.panelMeter.TabIndex = 13;
            // 
            // panelGauge
            // 
            this.panelGauge.BackgroundImage = global::drc.Properties.Resources.bg_meter022;
            this.panelGauge.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelGauge.Controls.Add(this.WattMeter);
            this.panelGauge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGauge.Location = new System.Drawing.Point(3, 3);
            this.panelGauge.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelGauge.Name = "panelGauge";
            this.panelGauge.Padding = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.panelGauge.Size = new System.Drawing.Size(194, 355);
            this.panelGauge.TabIndex = 25;
            // 
            // WattMeter
            // 
            this.WattMeter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.WattMeter.CaptionBackColor = System.Drawing.Color.Transparent;
            this.WattMeter.CaptionForeColor = System.Drawing.Color.Transparent;
            this.WattMeter.Controls.Add(this.labelwatt);
            this.WattMeter.DialColor = System.Drawing.Color.LightGray;
            this.WattMeter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WattMeter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WattMeter.ForeColor = System.Drawing.Color.DimGray;
            this.WattMeter.GaugeStyle = NationalInstruments.UI.GaugeStyle.SunkenWithThinNeedle;
            this.WattMeter.Location = new System.Drawing.Point(5, 6);
            this.WattMeter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.WattMeter.Name = "WattMeter";
            this.WattMeter.PointerColor = System.Drawing.Color.Red;
            this.WattMeter.Range = new NationalInstruments.UI.Range(0D, 300D);
            this.WattMeter.ScaleBaseLineColor = System.Drawing.Color.Transparent;
            this.WattMeter.Size = new System.Drawing.Size(184, 343);
            this.WattMeter.SpindleColor = System.Drawing.Color.OrangeRed;
            this.WattMeter.TabIndex = 18;
            // 
            // labelwatt
            // 
            this.labelwatt.AutoSize = true;
            this.labelwatt.BackColor = System.Drawing.Color.Transparent;
            this.labelwatt.Font = new System.Drawing.Font("Segoe UI", 6F);
            this.labelwatt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelwatt.Location = new System.Drawing.Point(77, 120);
            this.labelwatt.Name = "labelwatt";
            this.labelwatt.Size = new System.Drawing.Size(32, 12);
            this.labelwatt.TabIndex = 25;
            this.labelwatt.Text = "WATTs";
            // 
            // panelControl
            // 
            this.panelControl.Controls.Add(this.panelMeter1);
            this.panelControl.Controls.Add(this.panelFreqName1);
            this.panelControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl.Location = new System.Drawing.Point(0, 0);
            this.panelControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelControl.Name = "panelControl";
            this.panelControl.Size = new System.Drawing.Size(523, 393);
            this.panelControl.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 19);
            this.label6.TabIndex = 1;
            this.label6.Text = "000.00";
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Transparent;
            this.panel12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel12.BackgroundImage")));
            this.panel12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel12.Controls.Add(this.label6);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel12.Location = new System.Drawing.Point(0, 208);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(215, 31);
            this.panel12.TabIndex = 22;
            // 
            // GaugeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.panelControl);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "GaugeControl";
            this.Size = new System.Drawing.Size(523, 393);
            this.Load += new System.EventHandler(this.GaugeControl1_Load);
            this.Resize += new System.EventHandler(this.GaugeControl1_Resize);
            this.panelFreqName1.ResumeLayout(false);
            this.panelFreqName1.PerformLayout();
            this.panelMeter1.ResumeLayout(false);
            this.panelDisplay.ResumeLayout(false);
            this.panelRadialGauge.ResumeLayout(false);
            this.panelLed.ResumeLayout(false);
            this.panelMenu.ResumeLayout(false);
            this.panelDigital.ResumeLayout(false);
            this.panelBTN.ResumeLayout(false);
            this.tableLayoutPanelButton.ResumeLayout(false);
            this.panelHiLow.ResumeLayout(false);
            this.panelOnOff.ResumeLayout(false);
            this.tableLayoutPanelDigital.ResumeLayout(false);
            this.panelamp.ResumeLayout(false);
            this.panelamp.PerformLayout();
            this.panelvolt.ResumeLayout(false);
            this.panelvolt.PerformLayout();
            this.paneltemp.ResumeLayout(false);
            this.paneltemp.PerformLayout();
            this.panelWatt.ResumeLayout(false);
            this.panelWatt.PerformLayout();
            this.panellight.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureARU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.led_TX)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureARD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.led_RX)).EndInit();
            this.panelMeter.ResumeLayout(false);
            this.panelGauge.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WattMeter)).EndInit();
            this.WattMeter.ResumeLayout(false);
            this.WattMeter.PerformLayout();
            this.panelControl.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelwatt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panelFreqName1;
        private System.Windows.Forms.Label labelname;
        private System.Windows.Forms.Panel panelMeter1;
        private System.Windows.Forms.Panel panelDisplay;
        private System.Windows.Forms.Panel panelRadialGauge;
        private System.Windows.Forms.Panel panelLed;
        private System.Windows.Forms.Panel panelMeter;
        private System.Windows.Forms.Panel panelGauge;
        private NationalInstruments.UI.WindowsForms.Gauge WattMeter;
        private System.Windows.Forms.Panel panelControl;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Panel panellight;
        private System.Windows.Forms.Panel panelDigital;
        private System.Windows.Forms.Panel panelBTN;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelButton;
        private System.Windows.Forms.Panel panelHiLow;
        private EnhancedGlassButton.GlassButton HiLowBtn;
        private System.Windows.Forms.Panel panelOnOff;
        private EnhancedGlassButton.GlassButton OnOffBtn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelDigital;
        private System.Windows.Forms.Panel panelamp;
        private System.Windows.Forms.Label UnitAmp;
        private System.Windows.Forms.Label labelAmp1;
        private System.Windows.Forms.Panel panelvolt;
        private System.Windows.Forms.Label UnitVolt;
        private System.Windows.Forms.Label labelVolt1;
        private System.Windows.Forms.Panel paneltemp;
        private System.Windows.Forms.Label UnitTemp;
        private System.Windows.Forms.Label labelTemp1;
        private System.Windows.Forms.Panel panelWatt;
        private System.Windows.Forms.Label UnitWatt;
        private System.Windows.Forms.Label labelgauge;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox led_RX;
        private System.Windows.Forms.PictureBox pictureARD;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureARU;
        private System.Windows.Forms.PictureBox led_TX;
    }
}
