﻿namespace drc
{
    partial class SwitchControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelNameSwitch = new System.Windows.Forms.Panel();
            this.labelname = new System.Windows.Forms.Label();
            this.panelBody = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panelPicSwitch = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.led = new System.Windows.Forms.PictureBox();
            this.pictureBoxSwitch = new System.Windows.Forms.PictureBox();
            this.panelSwitch = new System.Windows.Forms.Panel();
            this.panelNameSwitch.SuspendLayout();
            this.panelBody.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panelPicSwitch.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.led)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSwitch)).BeginInit();
            this.panelSwitch.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelNameSwitch
            // 
            this.panelNameSwitch.BackColor = System.Drawing.Color.Transparent;
            this.panelNameSwitch.Controls.Add(this.labelname);
            this.panelNameSwitch.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelNameSwitch.Location = new System.Drawing.Point(0, 0);
            this.panelNameSwitch.Name = "panelNameSwitch";
            this.panelNameSwitch.Padding = new System.Windows.Forms.Padding(5);
            this.panelNameSwitch.Size = new System.Drawing.Size(266, 50);
            this.panelNameSwitch.TabIndex = 4;
            // 
            // labelname
            // 
            this.labelname.AutoSize = true;
            this.labelname.Font = new System.Drawing.Font("Segoe UI", 11.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelname.ForeColor = System.Drawing.Color.White;
            this.labelname.Location = new System.Drawing.Point(79, 10);
            this.labelname.Name = "labelname";
            this.labelname.Size = new System.Drawing.Size(75, 28);
            this.labelname.TabIndex = 5;
            this.labelname.Text = "Switch";
            // 
            // panelBody
            // 
            this.panelBody.BackColor = System.Drawing.Color.Transparent;
            this.panelBody.Controls.Add(this.panel4);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 50);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(266, 194);
            this.panelBody.TabIndex = 5;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panelPicSwitch);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(266, 194);
            this.panel4.TabIndex = 2;
            // 
            // panelPicSwitch
            // 
            this.panelPicSwitch.Controls.Add(this.panel6);
            this.panelPicSwitch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPicSwitch.Location = new System.Drawing.Point(0, 0);
            this.panelPicSwitch.Name = "panelPicSwitch";
            this.panelPicSwitch.Padding = new System.Windows.Forms.Padding(5);
            this.panelPicSwitch.Size = new System.Drawing.Size(266, 194);
            this.panelPicSwitch.TabIndex = 4;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel1);
            this.panel6.Controls.Add(this.pictureBoxSwitch);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(5, 5);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(5, 5, 40, 5);
            this.panel6.Size = new System.Drawing.Size(256, 184);
            this.panel6.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.led);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(196, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(20, 174);
            this.panel1.TabIndex = 7;
            // 
            // led
            // 
            this.led.Dock = System.Windows.Forms.DockStyle.Top;
            this.led.Image = global::drc.Properties.Resources.led_gray;
            this.led.Location = new System.Drawing.Point(0, 0);
            this.led.Name = "led";
            this.led.Size = new System.Drawing.Size(20, 33);
            this.led.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.led.TabIndex = 9;
            this.led.TabStop = false;
            // 
            // pictureBoxSwitch
            // 
            this.pictureBoxSwitch.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxSwitch.Image = global::drc.Properties.Resources.switch_on;
            this.pictureBoxSwitch.Location = new System.Drawing.Point(91, 3);
            this.pictureBoxSwitch.Name = "pictureBoxSwitch";
            this.pictureBoxSwitch.Size = new System.Drawing.Size(70, 176);
            this.pictureBoxSwitch.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxSwitch.TabIndex = 6;
            this.pictureBoxSwitch.TabStop = false;
            // 
            // panelSwitch
            // 
            this.panelSwitch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.panelSwitch.Controls.Add(this.panelBody);
            this.panelSwitch.Controls.Add(this.panelNameSwitch);
            this.panelSwitch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSwitch.Location = new System.Drawing.Point(0, 0);
            this.panelSwitch.Name = "panelSwitch";
            this.panelSwitch.Size = new System.Drawing.Size(266, 244);
            this.panelSwitch.TabIndex = 1;
            // 
            // SwitchControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelSwitch);
            this.Name = "SwitchControl";
            this.Size = new System.Drawing.Size(266, 244);
            this.Load += new System.EventHandler(this.SwitchControl1_Load);
            this.Resize += new System.EventHandler(this.SwitchControl1_Resize);
            this.panelNameSwitch.ResumeLayout(false);
            this.panelNameSwitch.PerformLayout();
            this.panelBody.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panelPicSwitch.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.led)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSwitch)).EndInit();
            this.panelSwitch.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelNameSwitch;
        private System.Windows.Forms.Label labelname;
        private System.Windows.Forms.Panel panelBody;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panelPicSwitch;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox led;
        private System.Windows.Forms.PictureBox pictureBoxSwitch;
        private System.Windows.Forms.Panel panelSwitch;
    }
}
