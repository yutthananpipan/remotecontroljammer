﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace drc.Model
{
    class modules
    {
        public byte id { get; set; }
        public string freq_name { get; set; }
        public int min_data { get; set; }
        public int max_data { get; set; }
        public bool enable { get; set; }
        public int comport { get; set; }

    }
}
