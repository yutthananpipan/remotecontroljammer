﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace drc
{
    class CustomColor
    {
        public CustomColor()
        {
            this.PowerOnColor = System.Drawing.ColorTranslator.FromHtml("#4CAF50");
            this.PowerOffColor = System.Drawing.ColorTranslator.FromHtml("#424242");

            this.HiColor = System.Drawing.ColorTranslator.FromHtml("#8BC34A");
            this.LoColor = System.Drawing.ColorTranslator.FromHtml("#CDDC39");

            this.GrayColor = System.Drawing.ColorTranslator.FromHtml("#424242");
            this.RedColor = System.Drawing.ColorTranslator.FromHtml("#C62828");

        }
        public Color PowerOnColor { get; set; }
        public Color PowerOffColor { get; set; }

        public Color HiColor { get; set; }
        public Color LoColor { get; set; }

        public Color GrayColor { get; set; }
        public Color RedColor { get; set; }

    }
}
