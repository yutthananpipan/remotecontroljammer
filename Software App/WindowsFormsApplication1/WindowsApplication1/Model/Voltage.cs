﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace drc.Model
{
    class voltage
    {
        public int min_data { get; set; }
        public int max_data { get; set; }
        public int danger_min_range { get; set; }
        public int danger_max_range { get; set; }
        public int warning_min_range { get; set; }
        public int warning_max_range { get; set; }
        public int normal_min_range { get; set; }
        public int normal_max_range { get; set; }


    }
}
