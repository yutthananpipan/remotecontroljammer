﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.ComponentModel;
using System.Windows.Forms;
using AquaControls;

namespace drc
{
    class CustomUpdateGauge
    {
        private delegate void DELEGATE();
        public CustomUpdateGauge(AquaGauge gauge, int value)
        {
            this.gauge = gauge;
            this.value = value;

            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();

            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
          
        }

        public void startAsyncButton_Click()
        {
            if (backgroundWorker1.IsBusy != true)
            {
                // Start the asynchronous operation.
                this.backgroundWorker1.RunWorkerAsync(100);
            }
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            // Do not access the form's BackgroundWorker reference directly.
            // Instead, use the reference provided by the sender parameter.
            BackgroundWorker bw = sender as BackgroundWorker;

            // Extract the argument.
            int arg = (int)e.Argument;

        }

        // This event handler demonstrates how to interpret 
        // the outcome of the asynchronous operation implemented
        // in the DoWork event handler.
        private void backgroundWorker1_RunWorkerCompleted(
            object sender,
            RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                // The user canceled the operation.
                //MessageBox.Show("Operation was canceled");
            }
            else if (e.Error != null)
            {
                //There was an error during the operation.
                //string msg = String.Format("An error occurred: {0}", e.Error.Message);
                //MessageBox.Show(msg);
            }
            else
            {
                
                // The operation completed normally.
                //string msg = String.Format("Result = {0}", e.Result);
                //MessageBox.Show(msg);
                try
                {
                    gauge.Invoke(new UpdateTextCallback(this.UpdateUi), new object[] { "Text generated on non-UI thread." });
                }
                catch { }
               
            }
        }

        private void UpdateUi(string text)
        {
            if (this.value == 0)
            {
                this.gauge.Value = 0;
                this.gauge.ThresholdPercent = 0.1F;

            }
            else
            {
                //for (int i = 0; i <= this.value; i+=this.value/2)
                //{
                //    //Thread.Sleep(1);
                //    this.gauge.Value = i;
                //    this.gauge.ThresholdPercent = this.gauge.Value * 100F / this.gauge.MaxValue;
                   
                //}

                this.gauge.Value = value;
                this.gauge.ThresholdPercent = this.gauge.Value * 100F / this.gauge.MaxValue;

            }
        }

        public int value { get; set; }
        public AquaGauge gauge { get; set; }
        private System.ComponentModel.BackgroundWorker backgroundWorker1;

        public delegate void UpdateTextCallback(string text);
    }
}
