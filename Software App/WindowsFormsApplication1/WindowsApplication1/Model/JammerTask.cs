﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO.Ports;
using System.Windows.Forms;
using static drc.MainForm;

namespace drc.Model
{
    class JammerTask
    {
        private MainForm mainForm;

        private SerialPort serialPort;
        private System.Windows.Forms.Timer timer_Interval_System;

        private int handler_interval = 300;
        private ManualResetEvent _manualResetEvent = new ManualResetEvent(true);

        private List<GaugeControl> list_Gauge = new List<GaugeControl>();

        private int comport = 1;

        // Constant divided
        private byte divvolt = 8;
        private byte divamp = 8;
        private byte divtemp = 2;

        //! Position Number byte Check Sum

        const byte ByteOfChksum = 9;

        //! Constant Command Pattern Interactive Control Board RF interface
        //! PC Trancieve
        private const byte TxCommandStart = 0xF0;
        private const byte TxCommandAllControl = 0xFF;
        private const byte TxCommandAnalogRead = 0xFA;
        private const byte TxCommandDigitalRead = 0xFD;
        private const byte TxCommandStop = 0xF1;
        //!
        public const byte TxPowerOnJammer = 0x11;
        public const byte TxPowerOffJammer = 0x00;
        public const byte TxLowPowerJammer = 0x11;
        public const byte TxHiPowerJammer = 0x00;
        //private const byte TxOnSkipJammer = 0x11;
        //private const byte TxOffSkipJammer = 0x00;
        private const byte TxReadBatteryJammer = 0x11;

        //! Rf Board Recieve

        private const byte RxBatteryFullJammer = 0x80;
        private const byte RxBatteryChargeJammer = 0x40;
        private const byte RxBatteryOperateJammer = 0x20;
        private const byte RxBatteryLowBattOperateJammer = 0x21;
        private const byte RxBatteryFailJammer = 0x10;

        private const byte RxCommandStart = 0xE0;
        private const byte RxCommandAnalogRead = 0xEA;
        private const byte RxCommandDigitalRead = 0xED;
        private const byte RxCommandStop = 0xE1;

        public const byte AntenaUp = 0xF0;
        public const byte AntenaDown = 0x0F;
        public const byte AntenaFloat = 0x00;

        private const byte RxPowerOnJammer = 0x00;
        private const byte RxPowerOffJammer = 0x11;
        private const byte RxHiPowerJammer = 0x00;
        private const byte RxLowPowerJammer = 0x11;
        private const byte RxOnSkipJammer = 0x00;
        private const byte RxOffSkipJammer = 0x11;

        /*
         * rx tx data
         * */
        byte[,] Data_RX_Buffer = new byte[1000, 6];
        public byte Data_TX_ID;
        public byte[,] Data_TX_Buffer = new byte[1000, 6];
        byte[,] Data_RX_Power = new byte[13, 4];
        byte[] RX_Buffer = new byte[10];
        byte[] TX_Buffer = new byte[10];

        byte System_Counter = 0;
        public Boolean System_Control = false;
        Boolean System_Read_Analog = false;
        byte RX_Counter = 0;
        byte TX_Counter = 0;
        int Power = 0;
        Boolean RX_Ready = false;

        static byte Buffer_Read_Volt = 0;
        static byte Buffer_Read_Amp = 1;
        static byte Buffer_Read_Watt = 2;
        static byte Buffer_Read_Temp = 3;
        static byte Buffer_Read_Mode = 4;

        public SerialPort SerialPort { get => serialPort; set => serialPort = value; }

        public JammerTask(MainForm main, List<GaugeControl> list_guage, int comport, System.ComponentModel.IContainer components, int interval)
        {
            this.mainForm = main;
            this.list_Gauge = list_guage;
            this.comport = comport;
            this.handler_interval = interval;

            // Update Data form serialport
            this.SerialPort = new SerialPort(components);
            this.SerialPort.PortName = "COM" + comport.ToString();
            this.SerialPort.WriteTimeout = 500;
            this.SerialPort.ReadTimeout = 500;
            this.SerialPort.DataReceived += new SerialDataReceivedEventHandler(this.serialPort_DataReceived);

            // Timer Update System
            this.timer_Interval_System = new System.Windows.Forms.Timer();
            this.timer_Interval_System.Enabled = true;
            this.timer_Interval_System.Interval = 1000;
            this.timer_Interval_System.Tick += new System.EventHandler(this.Timer_Interval_System_Tick);

            Data_TX_ID = 1;
            System_Counter = 0;
            System_Control = false;
            System_Read_Analog = true;

            SetTask2Module();

            Task task = Task.Factory.StartNew(() => HandlerDataChange());
            this.Pause();
        }

        public void OpenSerialPort()
        {
            try
            {
                this.SerialPort.Open();
            }
            catch (Exception) {
                
            }
        }

        public void CloseSerialPort()
        {
            try
            {
                this.SerialPort.Close();
            }
            catch (Exception)
            {

            }
        }

        private void SetTask2Module()
        {
            foreach (GaugeControl gaugeControl in list_Gauge)
            {
                gaugeControl.JammerTask = this;
            }
        }

        private void HandlerDataChange()
        {
            while (true)
            {
                string mode = "Analog";

                // update ui
                foreach (GaugeControl gaugeControl in this.list_Gauge)
                {
                    try
                    {
                        foreach (GaugeControl gauge_led in this.list_Gauge)
                        {
                            try
                            {
                                // SET LED OFF
                                this.mainForm.Invoke(new LEDDelegate(gauge_led.LED_RX_DataChange), new object[] { false });
                                this.mainForm.Invoke(new LEDDelegate(gauge_led.LED_TX_DataChange), new object[] { false });
                            }
                            catch (Exception)
                            {

                            }
                        }

                        if (gaugeControl.Enabled == false)
                        {
                            continue;
                        }

                        else
                        {
                            // Stop and wait to start
                            _manualResetEvent.WaitOne();
                        }

                        // PATTERN DATA Sent 
                        // Define Computer Digital
                        //Transmit to board
                        // 0XF0 | 0xFD | ID | Enable Status | Low Power Status | Skip Status | 0x00 | 0x00 | 0xF1 | Check Sum

                        if (System_Read_Analog == true)
                        {
                            if (gaugeControl.type == MainForm.Type.Antenna)
                            {
                                continue;
                            }

                            TX_Buffer[0] = TxCommandStart;
                            TX_Buffer[1] = TxCommandAnalogRead;

                            TX_Buffer[2] = gaugeControl.jammerID;
                            TX_Buffer[3] = 0X00;
                            TX_Buffer[4] = 0X00;
                            TX_Buffer[5] = 0X00;
                            TX_Buffer[6] = 0X00;
                            TX_Buffer[7] = 0X00;
                            TX_Buffer[8] = TxCommandStop;

                            mode = "Analog";

                        }
                        else
                        {
                            TX_Buffer[0] = TxCommandStart;
                            TX_Buffer[1] = TxCommandDigitalRead;

                            TX_Buffer[2] = Convert.ToByte(gaugeControl.jammerID);

                            TX_Buffer[3] = Data_TX_Buffer[gaugeControl.jammerID, GaugeControl.module_OnOff];
                            TX_Buffer[4] = Data_TX_Buffer[gaugeControl.jammerID, GaugeControl.module_HiLow];
                            TX_Buffer[5] = 0X00;
                            TX_Buffer[6] = 0X00;
                            TX_Buffer[7] = 0X00;
                            TX_Buffer[8] = TxCommandStop;

                            mode = "Digital";
                        }

                        System_Counter++;
                        if (System_Counter >= Convert.ToByte(this.list_Gauge.Count))
                        {
                            System_Counter = 0;
                            System_Read_Analog = !System_Read_Analog;
                        }

                        byte Sum = 0;
                        RX_Counter = 0;
                        for (TX_Counter = 0; TX_Counter < 9; TX_Counter++)
                        {

                            Sum += TX_Buffer[TX_Counter];

                            RX_Counter++;
                        }

                        TX_Buffer[9] = (byte)~Sum;
                        RX_Ready = false;

                        this.mainForm.Invoke(new LEDDelegate(gaugeControl.LED_TX_DataChange), new object[] { true });

                        Console.WriteLine("ID " + gaugeControl.jammerID.ToString() + " outing, mode [" + mode + "]");

                        if (this.SerialPort.IsOpen)
                        {
                            if (mainForm.mainswitchControl.switch_status == true || gaugeControl.type == MainForm.Type.Antenna)
                            {
                                this.SerialPort.Write(TX_Buffer, 0, 10);
                                Thread.Sleep(20);
                                this.SerialPort.DiscardOutBuffer();
                            }
                        }

                        this.Pause();

                        Thread.Sleep(handler_interval);

                    }
                    catch (Exception e)
                    {
                        e.ToString();
                        this.Pause();
                    }
                }
            }
        }

        public void Start()
        {
            if (_manualResetEvent != null)
            {
                _manualResetEvent.Set();
            }

        }

        public void Pause()
        {
            if (_manualResetEvent != null)
            {
                try
                {
                    _manualResetEvent.Reset();
                }
                catch (Exception e)
                {
                    e.ToString();
                }
            }
        }

        private void Timer_Interval_System_Tick(object sender, EventArgs e)
        {
            if (mainForm.mainswitchControl.switch_status)
            {
                this.Start();
            }
        }

        public void ShutdownSystem()
        {
            List<GaugeControl> gauges = list_Gauge.FindAll(i => i.type == MainForm.Type.JammerModule);

            foreach (GaugeControl gaugecontrol in gauges)
            {
                try
                {
                    System_Control = false;

                    if (Data_TX_Buffer[gaugecontrol.jammerID, GaugeControl.module_OnOff] == TxPowerOnJammer)
                    {
                        TX_Buffer[0] = TxCommandStart;
                        TX_Buffer[1] = TxCommandDigitalRead;

                        TX_Buffer[5] = 0X00;
                        TX_Buffer[6] = 0X00;
                        TX_Buffer[7] = 0X00;
                        TX_Buffer[8] = TxCommandStop;

                        // set off to any module
                        Data_TX_Buffer[gaugecontrol.jammerID, GaugeControl.module_OnOff] = TxPowerOffJammer;
                        Data_TX_Buffer[gaugecontrol.jammerID, GaugeControl.module_HiLow] = TxHiPowerJammer;
                        gaugecontrol.getStatusHi = true;

                        TX_Buffer[2] = gaugecontrol.jammerID;
                        TX_Buffer[3] = TxPowerOffJammer;
                        TX_Buffer[4] = TxHiPowerJammer;

                        byte Sum = 0;
                        RX_Counter = 0;
                        for (TX_Counter = 0; TX_Counter < 9; TX_Counter++)
                        {
                            Sum += TX_Buffer[TX_Counter];
                            RX_Counter++;
                        }

                        TX_Buffer[9] = (byte)~Sum;
                        RX_Ready = false;

                        SerialPort.Write(TX_Buffer, 0, 10);
                        Thread.Sleep(200);
                        SerialPort.DiscardOutBuffer();
                    }
                }
                catch (Exception e)
                {
                    e.ToString();
                }
            }

            try
            {
                this.SerialPort.DiscardOutBuffer();
            }
            catch (Exception)
            {

            }

            this.mainForm.ResetUI();
        }

        public void ControlAntena(byte id, bool state)
        {
            Pause();
            for (int i = 0; i < 3; i++)
            {
                if (SerialPort.IsOpen)
                {
                    Console.WriteLine(id.ToString() + " " + ((Data_TX_Buffer[id, GaugeControl.module_OnOff] == AntenaUp) ? "Antena => Up" : "Antena => Down"));
                    try
                    {
                        this.Pause();

                        Data_TX_Buffer[id, GaugeControl.module_OnOff] = (state) ? AntenaUp : AntenaDown;

                        TX_Buffer[0] = TxCommandStart;
                        TX_Buffer[1] = TxCommandDigitalRead;
                        TX_Buffer[2] = id;
                        TX_Buffer[3] = Data_TX_Buffer[id, GaugeControl.module_OnOff];
                        TX_Buffer[4] = 0x00;
                        TX_Buffer[5] = 0X00;
                        TX_Buffer[6] = 0X00;
                        TX_Buffer[7] = 0X00;
                        TX_Buffer[8] = TxCommandStop;

                        byte Sum = 0;
                        RX_Counter = 0;
                        for (TX_Counter = 0; TX_Counter < 9; TX_Counter++)
                        {
                            Sum += TX_Buffer[TX_Counter];
                            RX_Counter++;
                        }

                        TX_Buffer[9] = (byte)~Sum;
                        RX_Ready = false;

                        SerialPort.Write(TX_Buffer, 0, 10);
                        Thread.Sleep(200);
                        SerialPort.DiscardOutBuffer();
                    }
                    catch (Exception)
                    {

                    }
                }
            }

            Start();
        }

        public void ControlModule(byte id)
        {
            try
            {
                //TX
                if (System_Control == true)
                {
                    this.Pause();
                    //timer_Start();

                    TX_Buffer[0] = TxCommandStart;
                    TX_Buffer[1] = TxCommandDigitalRead;
                    TX_Buffer[2] = id;
                    TX_Buffer[3] = Data_TX_Buffer[id, GaugeControl.module_OnOff];
                    TX_Buffer[4] = Data_TX_Buffer[id, GaugeControl.module_HiLow];
                    TX_Buffer[5] = 0X00;
                    TX_Buffer[6] = 0X00;
                    TX_Buffer[7] = 0X00;
                    TX_Buffer[8] = TxCommandStop;
                }

                byte Sum = 0;
                RX_Counter = 0;
                for (TX_Counter = 0; TX_Counter < 9; TX_Counter++)
                {
                    Sum += TX_Buffer[TX_Counter];
                    RX_Counter++;
                }

                TX_Buffer[9] = (byte)~Sum;
                RX_Ready = false;

                SerialPort.Write(TX_Buffer, 0, 10);
                Thread.Sleep(20);
                SerialPort.DiscardOutBuffer();
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }

        private void System_Update_Display_Meter(int id, byte[,] datas)
        {
            Console.WriteLine("ID " + id.ToString() + " income");

            //static byte Buffer_Read_Volt = 0;
            //static byte Buffer_Read_Amp = 1;
            //static byte Buffer_Read_Watt = 2;
            //static byte Buffer_Read_Temp = 3;
            //static byte Buffer_Read_Mode = 4;
            GaugeControl gcontrol = list_Gauge.Find(i => i.jammerID == id);
            //meter volt and amp
            if (gcontrol.type == MainForm.Type.JammerModule)
            {
                if (gcontrol != null)
                {
                    string volt = string.Format("{0:f1}", (datas[id, Buffer_Read_Volt] / divvolt)).Trim();
                    string amp = string.Format("{0:f1}", (datas[id, Buffer_Read_Amp] / divamp)).Trim();
                    string temp = string.Format("{0:f1}", (datas[id, Buffer_Read_Temp] / divtemp)).Trim();

                    int watts = (int)datas[id, Buffer_Read_Watt];

                    this.mainForm.Invoke(new LEDDelegate(gcontrol.LED_RX_DataChange), new object[] { true });

                    //int gauge, string temp, string volt, string amp
                    this.mainForm.Invoke(new GaugeDelegate(gcontrol.GaugeDataChange), new object[] { watts, temp, volt, amp });

                    // update status button
                    /*if (watts > 0)
                    {
                        gcontrol.getStatusOn = true;
                        Data_TX_Buffer[gcontrol.jammerID, GaugeControl.module_OnOff] = TxPowerOnJammer;
                    }*/

                    gcontrol.UpdateButtonView();
                }

                int Sum_Amp = 0;
                List<GaugeControl> listGaugeControl = mainForm.GetListGuageControl;
                foreach (GaugeControl g in listGaugeControl)
                {
                    try
                    {
                        Sum_Amp += (int)g.getAmp();
                    }
                    catch (Exception) { }
                }

                // Sum Current
                this.mainForm.Invoke(new MeterDelegate(this.mainForm.ampControl.MeterDataChange), new object[] { Sum_Amp });

                if (this.mainForm.voltControl.getMeter.Value > 0)
                {
                    this.mainForm.shutdownControl.ChangePowerMode(ShutdownControl.PowerMode.AC);
                }
            }
            else
            {
                // Volt Meter
                this.mainForm.Invoke(new MeterDelegate(this.mainForm.voltControl.MeterDataChange), new object[] { (int)(datas[id, Buffer_Read_Volt] / divvolt) });

                // Battery Mode
                #region

                if (datas[id, Buffer_Read_Mode] == 1)  //  
                {

                    //bool Operate, bool Ready, bool Charge, bool NoBatt
                    //this.mainForm.Invoke(new BatteryModeDelegate(this.mainForm.battmodeControl.LEDDataChange), new object[] { false, false, false, false });
                    this.mainForm.shutdownControl.ChangePowerMode(ShutdownControl.PowerMode.AC);
                    this.mainForm.JammerBattery.Charging = false;
                }
                else if ((datas[id, Buffer_Read_Mode] == 2) || (datas[id, Buffer_Read_Mode] == 3))
                {

                    //bool Operate, bool Ready, bool Charge, bool NoBatt
                    //this.mainForm.Invoke(new BatteryModeDelegate(this.mainForm.battmodeControl.LEDDataChange), new object[] { false, false, true, true });
                    this.mainForm.shutdownControl.ChangePowerMode(ShutdownControl.PowerMode.Battery);
                    this.mainForm.JammerBattery.Charging = true;

                }
                else if (datas[id, Buffer_Read_Mode] == 4)
                {

                    //bool Operate, bool Ready, bool Charge, bool NoBatt
                    //this.mainForm.Invoke(new BatteryModeDelegate(this.mainForm.battmodeControl.LEDDataChange), new object[] { false, true, false, false });
                    this.mainForm.shutdownControl.ChangePowerMode(ShutdownControl.PowerMode.Battery);
                    this.mainForm.JammerBattery.Charging = false;
                }
                else if (datas[id, Buffer_Read_Mode] == 5)
                {

                    //bool Operate, bool Ready, bool Charge, bool NoBatt
                    //this.mainForm.Invoke(new BatteryModeDelegate(this.mainForm.battmodeControl.LEDDataChange), new object[] { true, false, false, true });
                    this.mainForm.shutdownControl.ChangePowerMode(ShutdownControl.PowerMode.AC);
                    this.mainForm.JammerBattery.Charging = false;
                }
                else if (datas[id, Buffer_Read_Mode] == 6)
                {

                    //bool Operate, bool Ready, bool Charge, bool NoBatt
                    //this.mainForm.Invoke(new BatteryModeDelegate(this.mainForm.battmodeControl.LEDDataChange), new object[] { true, false, false, true });
                    this.mainForm.shutdownControl.ChangePowerMode(ShutdownControl.PowerMode.AC);
                    this.mainForm.JammerBattery.Charging = false;
                }
                else if (datas[id, Buffer_Read_Mode] == 7)
                {

                    //bool Operate, bool Ready, bool Charge, bool NoBatt
                    //this.mainForm.Invoke(new BatteryModeDelegate(this.mainForm.battmodeControl.LEDDataChange), new object[] { true, false, false, true });
                    this.mainForm.shutdownControl.ChangePowerMode(ShutdownControl.PowerMode.AC);
                    this.mainForm.JammerBattery.Charging = false;
                }

                this.mainForm.JammerBattery.OnBatteryValueChange((int)datas[id, Buffer_Read_Watt]);
                #endregion

            }

        }

        private void System_Update_Display_Button(byte[] rX_Buffer)
        {
            byte id = rX_Buffer[2];
            Console.WriteLine("ID " + id.ToString() + " income");

            try
            {
                GaugeControl gcontrol = list_Gauge.Find(i => i.jammerID == id);
                if (gcontrol != null)
                {
                    this.mainForm.Invoke(new LEDDelegate(gcontrol.LED_RX_DataChange), new object[] { true });

                    // State On/Off
                    if (Data_TX_Buffer[id, GaugeControl.module_OnOff] == TxPowerOnJammer)
                    {
                        gcontrol.getStatusOn = true;
                        this.Data_TX_Buffer[id, GaugeControl.module_OnOff] = JammerTask.TxPowerOnJammer;
                    }
                    else
                    {
                        gcontrol.getStatusOn = false;
                        this.Data_TX_Buffer[id, GaugeControl.module_OnOff] = JammerTask.TxPowerOffJammer;
                    }

                    // State Hi/Low
                    if (Data_TX_Buffer[id, GaugeControl.module_HiLow] == TxHiPowerJammer)
                    {
                        gcontrol.getStatusHi = true;
                        this.Data_TX_Buffer[id, GaugeControl.module_HiLow] = JammerTask.TxHiPowerJammer;
                    }
                    else
                    {
                        gcontrol.getStatusHi = false;
                        this.Data_TX_Buffer[id, GaugeControl.module_HiLow] = JammerTask.TxLowPowerJammer;
                    }

                    gcontrol.UpdateButtonView();

                    if (gcontrol.type == MainForm.Type.Antenna)
                    {
                        if (rX_Buffer[4] == AntenaUp)
                        {
                            bool state = true;
                            this.mainForm.antenaControl.switch_status = state;
                            this.mainForm.antenaControl.SwitchcOn(state);

                            Data_TX_Buffer[id, GaugeControl.module_OnOff] = 0x00;
                        }
                        else if (rX_Buffer[4] == AntenaDown)
                        {
                            bool state = false;
                            this.mainForm.antenaControl.switch_status = state;
                            this.mainForm.antenaControl.SwitchcOn(state);

                            Data_TX_Buffer[id, GaugeControl.module_OnOff] = 0x00;
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        private void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                Thread.Sleep(20);

                if (this.mainForm.mainswitchControl.switch_status == true)
                {
                    // PATTERN DATA RECEIVE 
                    // 0XE0 | 0xED | ID | Voltage | Current | Forward Power | Temp | VSWR | 0xE1 | Check Sum

                    int readByte = SerialPort.BytesToRead;
                    byte[] data = new byte[readByte];

                    Array.Clear(data, 0, readByte);
                    Array.Resize(ref RX_Buffer, readByte);

                    if (readByte == 10)
                    {
                        RecieveDataEvent fireArg = new RecieveDataEvent();
                        fireArg._NumberDataRecieve = readByte;
                        Array.Clear(fireArg._Buffer, 0, fireArg._Buffer.Length);

                        for (ushort i = 0; i < readByte; i++)
                        {
                            data[i] = ((byte)SerialPort.ReadByte());

                        }
                    }

                    for (int i = 0; i < RX_Buffer.Length; i++)
                    {
                        RX_Buffer[i] = data[i];
                    }

                    if ((RX_Buffer[0] == RxCommandStart) && (RX_Buffer[8] == RxCommandStop))
                    {
                        byte Sum = 0;
                        for (RX_Counter = 0; RX_Counter < 9; RX_Counter++)
                        {
                            Sum += RX_Buffer[RX_Counter];

                        }

                        if (RX_Buffer[9] == (byte)~Sum)
                        {
                            RX_Ready = true;

                            this.mainForm.Invoke((MethodInvoker)delegate
                            {
                                // RX
                                try
                                {
                                    //Read Status Volt Amp Meter Temp...
                                    if ((RX_Buffer[1] == RxCommandAnalogRead) && (RX_Ready == true))
                                    {
                                        // RX_Buffer[2] is ID
                                        Data_RX_Buffer[RX_Buffer[2], Buffer_Read_Volt] = RX_Buffer[3];   // volt
                                        Data_RX_Buffer[RX_Buffer[2], Buffer_Read_Amp] = RX_Buffer[4];   // Amp
                                        Data_RX_Buffer[RX_Buffer[2], Buffer_Read_Watt] = RX_Buffer[5];   // Watt
                                        Data_RX_Buffer[RX_Buffer[2], Buffer_Read_Temp] = RX_Buffer[6];   // Temp
                                        Data_RX_Buffer[RX_Buffer[2], Buffer_Read_Mode] = RX_Buffer[7]; //mode

                                        System_Update_Display_Meter(RX_Buffer[2], Data_RX_Buffer);

                                    }

                                    //Read Status Button
                                    else if ((RX_Buffer[1] == RxCommandDigitalRead) && (RX_Ready == true))
                                    {
                                        Data_TX_Buffer[RX_Buffer[2], GaugeControl.module_OnOff] = RX_Buffer[3];
                                        Data_TX_Buffer[RX_Buffer[2], GaugeControl.module_HiLow] = RX_Buffer[4];
                                        System_Update_Display_Button(RX_Buffer);

                                    }
                                }
                                catch
                                {
                                    //ResetAllDisplay();
                                    Console.WriteLine("Error");
                                }
                                finally
                                {
                                    System_Control = false; // button off

                                }
                            });

                        }
                        else
                        {
                            RX_Ready = false;
                        }
                    }
                    else
                    {
                        RX_Ready = false;
                    }
                }
            }
            catch (IndexOutOfRangeException)
            {
                RX_Ready = false;
                Console.WriteLine("Error");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                SerialPort.DiscardInBuffer();
                this.Start();
            }
        }

    }
}
