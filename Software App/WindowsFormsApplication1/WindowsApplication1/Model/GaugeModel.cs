﻿using AquaControls;
using System.Windows.Forms;

namespace drc
{
    class GaugeModel
    {
        public GaugeModel(Panel panel, AquaGauge guage, Button hilow_btn, Button onof_btn, TextBox temp_textbox, TextBox volt_textbox, TextBox amp_textbox, PictureBox pictureTemp, PictureBox pictureVolt, PictureBox pictureAmp)
        {
            this.panel = panel;
            this.gauge = guage;
            this.hilow_btn = hilow_btn;
            this.onof_btn = onof_btn;
            this.temp_textbox = temp_textbox;
            this.volt_textbox = volt_textbox;
            this.amp_textbox = amp_textbox;

            this.pictureTemp = pictureTemp;
            this.pictureVolt = pictureVolt;
            this.pictureAmp = pictureAmp;

            this.DisplayCount = 0;

        }

        public int DisplayCount { get; set; }
        public Panel panel { get; set; }
        public AquaGauge gauge { get; set; }
        public Button hilow_btn { get; set; }
        public Button onof_btn { get; set; }
        public TextBox temp_textbox { get; set; }
        public TextBox volt_textbox { get; set; }
        public TextBox amp_textbox { get; set; }
        public PictureBox pictureTemp {get; set; }
        public PictureBox pictureVolt {get; set; }
        public PictureBox pictureAmp { get; set; }
    }
}
