﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace drc
{
    class FeqModel
    {
        public FeqModel(Panel panel, Label label)
        {
            this.Panel = panel;
            this.Label = label;
        }
        public Label Label { get; set; }
        public Panel Panel { get; set; }
    }
}
