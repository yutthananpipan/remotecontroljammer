﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace drc
{
    class BaterryModel
    {
        public BaterryModel()
        {
            this.listBattery = new List<Bitmap>();
            this.CountBitmap = 0;
            listBattery.Add(global::drc.Properties.Resources.batteryChart10);
            listBattery.Add(global::drc.Properties.Resources.batteryChart20);
            listBattery.Add(global::drc.Properties.Resources.batteryChart40);
            listBattery.Add(global::drc.Properties.Resources.batteryChart60);
            listBattery.Add(global::drc.Properties.Resources.batteryChart80);
            listBattery.Add(global::drc.Properties.Resources.batteryChart100);

        }

        public List<Bitmap> listBattery {get; set;}
        public int CountBitmap { get; set; }
    }
}
