﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace drc
{
    public partial class PercenBatt : UserControl
    {
        public bool Charging = false;
        private delegate void PercentDelegate(int value);
        public enum BatteryType : int { PC = 0, Jammer = 1 }

        private BatteryType type;

        public PercenBatt(BatteryType type)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this.type = type;

            updatePercent(0);
        }

        private void PercenBatt_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            InitLocationLayout();
        }

        private void updatePercent(int value)
        {
            int level = (int)(value / 20);

            if (level > 4)
            {
                this.BatteryIMG.Image = type == BatteryType.PC ? global::drc.Properties.Resources.batt100 : global::drc.Properties.Resources.batteryChart100;
            }
            else if (level > 3)
            {
                this.BatteryIMG.Image = type == BatteryType.PC ? global::drc.Properties.Resources.batt80 : global::drc.Properties.Resources.batteryChart80;
            }
            else if (level > 2)
            {
                this.BatteryIMG.Image = type == BatteryType.PC ? global::drc.Properties.Resources.batt60 : global::drc.Properties.Resources.batteryChart60;
            }
            else if (level > 1)
            {
                this.BatteryIMG.Image = type == BatteryType.PC ? global::drc.Properties.Resources.batt40 : global::drc.Properties.Resources.batteryChart40;
            }
            else
            {
                if (value >= 11)
                {
                    this.BatteryIMG.Image = type == BatteryType.PC ? global::drc.Properties.Resources.batt20 : global::drc.Properties.Resources.batteryChart20;
                }
                else if (value >= 6 && value <= 10)
                {
                    this.BatteryIMG.Image = type == BatteryType.PC ? global::drc.Properties.Resources.batt10 : global::drc.Properties.Resources.batteryChart10;
                }
                else if (value >= 1 && value <= 5)
                {
                    this.BatteryIMG.Image = type == BatteryType.PC ? global::drc.Properties.Resources.battlow : global::drc.Properties.Resources.batterylow;
                }
                else
                {
                    this.BatteryIMG.Image = type == BatteryType.PC ? global::drc.Properties.Resources.batt0 : global::drc.Properties.Resources.battery0;
                }

            }

            if (Charging)
            {
                if (level > 1)
                {
                    this.BatteryIMG.Image = global::drc.Properties.Resources.battery_high_charging;
                }
                else
                {
                    this.BatteryIMG.Image = global::drc.Properties.Resources.battery_low_charging;
                }
            }

            SetPowerStatus();

            this.labelPercent.Text = value.ToString() + "%";
            InitLocationLayout();

        }

        public void OnBatteryValueChange(int value)
        {
            try
            {
                Invoke(new PercentDelegate(updatePercent), new object[] { value });
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }


        private void SetPowerStatus()
        {
            string battery_status = String.Empty;
            PowerStatus status = SystemInformation.PowerStatus;
            battery_status = status.BatteryChargeStatus.ToString();

            if (type == BatteryType.PC){

                if (battery_status.Contains("Charging") && battery_status.Contains("High"))
                {
                    this.BatteryIMG.Image = global::drc.Properties.Resources.batt_high_charging;
                }
                else if (battery_status.Contains("Charging") && battery_status.Contains("Low"))
                {
                    this.BatteryIMG.Image = global::drc.Properties.Resources.batt_low_charging;
                }
                else if (battery_status.Contains("Charging"))
                {
                    this.BatteryIMG.Image = global::drc.Properties.Resources.batt_high_charging;
                }
                else if (battery_status == "Low")
                {
                    this.BatteryIMG.Image = global::drc.Properties.Resources.battlow;
                }
            }

        }


        private void InitLocationLayout()
        {
            this.panelLayout.Location = new System.Drawing.Point((this.panelLayout.Parent.Width - this.panelLayout.Width) / 2, (this.panelLayout.Parent.Height - this.panelLayout.Height) / 2);
            //this.labelname.Location = new System.Drawing.Point(this.panelHeader.Width/2 - this.labelname.Width/2, this.panelHeader.Height/2 - this.labelname.Height/2);
            this.labelPercent.Location = new System.Drawing.Point(this.panelPercent.Width / 2 - this.labelPercent.Width / 2, this.panelPercent.Height / 2 / 2);
        }
    }
}
