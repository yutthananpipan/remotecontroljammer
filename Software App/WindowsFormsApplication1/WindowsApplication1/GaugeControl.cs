﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EnhancedGlassButton;
using System.Threading;
using NationalInstruments.UI.WindowsForms;
using drc.Model;
using static drc.MainForm;
using System.Globalization;

namespace drc
{
    public partial class GaugeControl : UserControl
    {
        public delegate void ButtonDelegate();

        private MainForm main;
        private bool StatusOn = false;
        private bool StatusHi = true;
        private byte ID = 99;
        private int comport = 1;
        public MainForm.Type type = 0;

        private JammerTask jammerTask;

        //Command Button Control
        public static int module_OnOff = 1;
        public static int module_HiLow = 2;
        public static int module_SkipBand = 3;

        public int Comport
        {
            get { return this.comport; }
            set { this.comport = value; }
        }

        public byte jammerID
        {
            get { return this.ID; }
            set { this.ID = value; }
        }

        public bool getStatusOn
        {
            get { return this.StatusOn; }
            set { this.StatusOn = value; }
        }

        public PictureBox getLed_RX
        {
            get { return this.led_RX; }
            set { this.led_RX = value; }
        }

        public PictureBox getLed_TX
        {
            get { return this.led_TX; }
            set { this.led_TX = value; }
        }

        public bool getStatusHi
        {
            get { return this.StatusHi; }
            set { this.StatusHi = value; }
        }

        public GlassButton getOnOffBtn
        {
            get { return OnOffBtn; }
        }

        public GlassButton getHiLowBtn
        {
            get { return HiLowBtn; }
        }

        public Gauge getGauge
        {
            get { return WattMeter; }
            set { this.WattMeter = value; }
        }

        public Label getFregName
        {
            get { return labelname; }
        }

        public Panel getpanelGauge
        {
            get { return panelGauge; }
        }

        internal JammerTask JammerTask { get => jammerTask; set => jammerTask = value; }

        public GaugeControl()
        {
            InitializeComponent();
        }

        public GaugeControl(MainForm main, byte id, int comport, MainForm.Type type)
        {
            InitializeComponent();
            this.main = main;
            this.ID = id;
            this.comport = comport;
            this.type = type;
        }

        public GaugeControl(MainForm main, byte id, string freqname, int min_data, int max_data, bool enable, int comport, MainForm.Type type)
        {
            InitializeComponent();
            this.main = main;
            this.ID = id;
            this.labelname.Text = freqname;
            this.WattMeter.Range = new NationalInstruments.UI.Range(Convert.ToDouble(min_data), Convert.ToDouble(max_data));
            this.Enabled = enable;
            this.comport = comport;
            this.type = type;

            this.StatusHi = true;

            SwitchChange();

        }

        private void GaugeControl1_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            InitLocationLayout();

        }

        private void InitLocationLayout()
        {
            //int led_height = panel3.Height + panel4.Height + panel13.Height;
            //this.panelDigital.Location = new System.Drawing.Point((this.panelDigital.Parent.Width - this.panelDigital.Width) / 2, (this.panelDigital.Parent.Height - this.panelDigital.Height) / 2);
            //this.labelgauge.Location = new System.Drawing.Point((this.labelgauge.Parent.Width - this.labelgauge.Width) / 2 + 4, (this.labelgauge.Parent.Height) - (this.labelgauge.Parent.Height / 3));
            //this.labelwatt.Location = new System.Drawing.Point((this.panelGauge.Width - this.labelwatt.Width) / 2, (this.panelGauge.Height) / 2 - (this.panelGauge.Height) / 4);
            this.labelname.Location = new System.Drawing.Point((int)(this.panelFreqName1.Width / 2) - (int)(this.labelname.Width / 2), (this.panelFreqName1.Height / 2) - (this.labelname.Height / 2));

            //this.panellight.Location = new System.Drawing.Point((this.panellight.Parent.Width - this.panellight.Width) / 2, (this.panelDigital.Location.Y / 2) - this.panellight.Height + 10);

            //value display

            this.labelwatt.Location = new System.Drawing.Point(this.labelwatt.Parent.Location.X + (this.labelwatt.Parent.Width / 2) - (this.labelwatt.Width / 2) - 2, this.labelwatt.Parent.Location.Y + (this.labelwatt.Parent.Height / 2) - 25);

            this.labelTemp1.Padding = new System.Windows.Forms.Padding(0, (this.paneltemp.Height / 2) - (this.labelTemp1.Height / 2) + 3, 0, 0);
            this.labelVolt1.Padding = new System.Windows.Forms.Padding(0, (this.panelvolt.Height / 2) - (this.labelVolt1.Height / 2) + 3, 0, 0);
            this.labelAmp1.Padding = new System.Windows.Forms.Padding(0, (this.panelamp.Height / 2) - (this.labelAmp1.Height / 2) + 3, 0, 0);
            this.labelgauge.Padding = new System.Windows.Forms.Padding(0, (this.panelWatt.Height / 2) - (this.labelgauge.Height / 2) + 3, 0, 0);

            this.UnitTemp.Padding = new System.Windows.Forms.Padding(0, (int)(paneltemp.Height / 2 - this.UnitTemp.Height / 2) + 3, 0, 0);
            this.UnitVolt.Padding = new System.Windows.Forms.Padding(0, (int)(panelvolt.Height / 2 - this.UnitVolt.Height / 2) + 3, 0, 0);
            this.UnitAmp.Padding = new System.Windows.Forms.Padding(0, (int)(panelamp.Height / 2 - this.UnitAmp.Height / 2) + 3, 0, 0);
            this.UnitWatt.Padding = new System.Windows.Forms.Padding(0, (int)(panelWatt.Height / 2 - this.UnitWatt.Height / 2) + 3, 0, 0);

            this.panelDigital.Padding = new System.Windows.Forms.Padding(3, (int)(this.panelDigital.Height - (panelBTN.Height + tableLayoutPanelDigital.Height + 3)) / 2, 3, 3);
        }

        public void Message(string text)
        {
            DialogResult dr = new DialogResult();
            WarningForm dialogfrm = new WarningForm();
            dialogfrm.getText.Text = text;
            dialogfrm.InitLocationLayout();
            dr = dialogfrm.ShowDialog();
        }

        private void UpdateUI(int gauge, string temp, string volt, string amp)
        {
            try
            {
                this.WattMeter.Value = Convert.ToDouble(gauge);
                if (this.labelgauge.InvokeRequired)
                {
                    this.labelgauge.BeginInvoke((MethodInvoker)delegate () { this.labelgauge.Text = gauge.ToString(); ; });
                }
                else
                {
                    this.labelgauge.Text = amp.ToString(); ;
                }

                if (this.labelTemp1.InvokeRequired)
                {
                    this.labelTemp1.BeginInvoke((MethodInvoker)delegate () { this.labelTemp1.Text = temp.ToString(); ; });
                }
                else
                {
                    this.labelTemp1.Text = temp.ToString(); ;
                }

                if (this.labelVolt1.InvokeRequired)
                {
                    this.labelVolt1.BeginInvoke((MethodInvoker)delegate () { this.labelVolt1.Text = volt.ToString(); ; });
                }
                else
                {
                    this.labelVolt1.Text = volt.ToString(); ;
                }

                if (this.labelAmp1.InvokeRequired)
                {
                    this.labelAmp1.BeginInvoke((MethodInvoker)delegate () { this.labelAmp1.Text = amp.ToString(); ; });
                }
                else
                {
                    this.labelAmp1.Text = amp.ToString(); ;
                }


            }
            catch (Exception) { }
        }

        public void GaugeDataChange(int gauge, string temp, string volt, string amp)
        {
            Thread thread = new Thread(() => UpdateUI(gauge, temp, volt, amp));
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            while (thread.IsAlive)
            {
                Application.DoEvents();
            }
        }

        private void OnBTNDataChange()
        {
            Invoke((MethodInvoker)delegate
            {
                if (Enabled)
                {
                    bool status = this.main.mainswitchControl.switch_status;

                    this.OnOffBtn.BackColor = (StatusOn) ? Color.Lime : Color.FromArgb(200, 200, 200);
                    this.OnOffBtn.GlowColor = (StatusOn) ? Color.Lime : Color.FromArgb(200, 200, 200);
                    this.OnOffBtn.ForeColor = (StatusOn) ? Color.White : Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
                    this.OnOffBtn.Text = (StatusOn) ? "ON" : "OFF";

                    this.HiLowBtn.BackColor = (StatusHi) ? Color.Lime : Color.Orange;
                    this.HiLowBtn.GlowColor = (StatusHi) ? Color.Lime : Color.DarkOrange;
                    this.HiLowBtn.ForeColor = Color.White;

                    this.HiLowBtn.Text = (StatusHi) ? "HI" : "LOW";
                }
            });

        }

        public void LED_RX_DataChange(bool IsOn)
        {
            this.led_RX.Image = (IsOn) ? global::drc.Properties.Resources.led_green : global::drc.Properties.Resources.led_gray;

        }

        public void LED_TX_DataChange(bool IsOn)
        {
            this.led_TX.Image = (IsOn) ? global::drc.Properties.Resources.led_red : global::drc.Properties.Resources.led_gray;

        }

        public void SwitchChange()
        {
            if (this.Enabled)
            {
                OnBTNDataChange();

                bool IsSwitchOn = this.main.mainswitchControl.switch_status;

                this.WattMeter.DialColor = (IsSwitchOn) ? System.Drawing.Color.SeaShell : System.Drawing.Color.LightGray;
                this.WattMeter.Value = (IsSwitchOn) ? this.WattMeter.Value : 0;

                this.WattMeter.ForeColor = (IsSwitchOn) ? System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64))))) : System.Drawing.Color.DimGray;
                this.OnOffBtn.ForeColor = (IsSwitchOn) ? System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64))))) : System.Drawing.Color.DimGray;

                this.HiLowBtn.BackColor = (IsSwitchOn) ? Color.Lime : Color.FromArgb(200, 200, 200);
                this.HiLowBtn.GlowColor = (IsSwitchOn) ? Color.Lime : Color.FromArgb(200, 200, 200);
                this.HiLowBtn.ForeColor = (IsSwitchOn) ? (StatusHi ? Color.White : System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))))) : System.Drawing.Color.DimGray;

                this.labelTemp1.ForeColor = (IsSwitchOn) ? System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64))))) : System.Drawing.Color.DimGray;
                this.UnitTemp.ForeColor = (IsSwitchOn) ? System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64))))) : System.Drawing.Color.DimGray;
                this.labelAmp1.ForeColor = (IsSwitchOn) ? System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64))))) : System.Drawing.Color.DimGray;
                this.UnitAmp.ForeColor = (IsSwitchOn) ? System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64))))) : System.Drawing.Color.DimGray;
                this.labelwatt.ForeColor = (IsSwitchOn) ? System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64))))) : System.Drawing.Color.DimGray;
                this.UnitWatt.ForeColor = (IsSwitchOn) ? System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64))))) : System.Drawing.Color.DimGray;
                this.labelVolt1.ForeColor = (IsSwitchOn) ? System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64))))) : System.Drawing.Color.DimGray;
                this.UnitVolt.ForeColor = (IsSwitchOn) ? System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64))))) : System.Drawing.Color.DimGray;

                this.labelname.ForeColor = (IsSwitchOn) ? System.Drawing.Color.SeaShell : System.Drawing.Color.DimGray;

                this.paneltemp.BackColor = (IsSwitchOn) ? System.Drawing.Color.SeaShell : System.Drawing.Color.Gainsboro;
                this.panelamp.BackColor = (IsSwitchOn) ? System.Drawing.Color.SeaShell : System.Drawing.Color.Gainsboro;
                this.panelWatt.BackColor = (IsSwitchOn) ? System.Drawing.Color.SeaShell : System.Drawing.Color.Gainsboro;
                this.panelvolt.BackColor = (IsSwitchOn) ? System.Drawing.Color.SeaShell : System.Drawing.Color.Gainsboro;

                this.pictureARU.Image = (IsSwitchOn) ? global::drc.Properties.Resources.arrow_up_red : global::drc.Properties.Resources.arrow_up_gray;
                this.pictureARD.Image = (IsSwitchOn) ? global::drc.Properties.Resources.arrow_down_green : global::drc.Properties.Resources.arrow_down_gray;

            }
        }

        private void GaugeControl1_Resize(object sender, EventArgs e)
        {
            InitLocationLayout();
        }

        public void UpdateButtonView()
        {
            try
            {
                if (this.main.mainswitchControl.switch_status == true && this.Enabled)
                {
                    OnBTNDataChange();

                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }

        public void HiLowBtn_Click(object sender, EventArgs e)
        {

            if (this.main.mainswitchControl.switch_status == true && this.jammerTask.Data_TX_Buffer[ID, module_OnOff] == JammerTask.TxPowerOnJammer && this.Enabled)
            {
                if (this.jammerTask.Data_TX_Buffer[ID, module_HiLow] == JammerTask.TxLowPowerJammer)
                {
                    this.jammerTask.Data_TX_Buffer[ID, module_HiLow] = JammerTask.TxHiPowerJammer;
                    StatusHi = true;
                }
                else if (this.jammerTask.Data_TX_Buffer[ID, module_HiLow] == JammerTask.TxHiPowerJammer)
                {
                    this.jammerTask.Data_TX_Buffer[ID, module_HiLow] = JammerTask.TxLowPowerJammer;
                    StatusHi = false;
                }

                this.jammerTask.System_Control = true;
                this.jammerTask.Data_TX_ID = ID;

                UpdateButtonView();
                this.jammerTask.ControlModule(ID);
            }
            else
            {
                Message("เมนสวิตช์หรือสวิตช์แจมเมอร์ถูกปิดอยู่ \n เปิดปุ่มดังกล่าวเพื่อใช้งานปุ่มนี้");
            }
        }

        public void OnOffBtn_Click(object sender, EventArgs e)
        {
            if (this.main.mainswitchControl.switch_status == true && this.Enabled)
            {
                if (this.main.antenaControl.switch_status == true)
                {
                    if (this.jammerTask.Data_TX_Buffer[ID, module_OnOff] == JammerTask.TxPowerOnJammer)
                    {
                        this.jammerTask.Data_TX_Buffer[ID, module_OnOff] = JammerTask.TxPowerOffJammer;
                        StatusOn = false;
                    }
                    else if (this.jammerTask.Data_TX_Buffer[ID, module_OnOff] == JammerTask.TxPowerOffJammer)
                    {
                        this.jammerTask.Data_TX_Buffer[ID, module_OnOff] = JammerTask.TxPowerOnJammer;
                        StatusOn = true;
                    }

                    this.jammerTask.System_Control = true;
                    this.jammerTask.Data_TX_ID = ID;

                    UpdateButtonView();
                    this.jammerTask.ControlModule(ID);
                }
                else
                {
                    Message("กรุณายกเสาก่อนเปิดใช้งานแจมเมอร์");
                }
            }
        }

        public int getAmp()
        {
            int amp = 0;
            try
            {
                amp = (int)float.Parse(labelAmp1.Text, CultureInfo.InvariantCulture.NumberFormat);
            }
            catch (Exception e) {
                e.ToString();
            }
            return amp;
        }
    }
}
