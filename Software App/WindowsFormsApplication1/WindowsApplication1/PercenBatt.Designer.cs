﻿namespace drc
{
    partial class PercenBatt
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelLayout = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panelPercent = new System.Windows.Forms.Panel();
            this.labelPercent = new System.Windows.Forms.Label();
            this.panelPB = new System.Windows.Forms.Panel();
            this.BatteryIMG = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panelLayout.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelPercent.SuspendLayout();
            this.panelPB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BatteryIMG)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Controls.Add(this.panelLayout);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(240, 149);
            this.panel1.TabIndex = 7;
            // 
            // panelLayout
            // 
            this.panelLayout.Controls.Add(this.panel2);
            this.panelLayout.Controls.Add(this.panelPercent);
            this.panelLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLayout.Location = new System.Drawing.Point(0, 0);
            this.panelLayout.Name = "panelLayout";
            this.panelLayout.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.panelLayout.Size = new System.Drawing.Size(240, 149);
            this.panelLayout.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.BatteryIMG);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(10, 10, 10, 0);
            this.panel2.Size = new System.Drawing.Size(240, 109);
            this.panel2.TabIndex = 5;
            // 
            // panelPercent
            // 
            this.panelPercent.Controls.Add(this.labelPercent);
            this.panelPercent.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelPercent.Location = new System.Drawing.Point(0, 109);
            this.panelPercent.Name = "panelPercent";
            this.panelPercent.Size = new System.Drawing.Size(240, 30);
            this.panelPercent.TabIndex = 4;
            // 
            // labelPercent
            // 
            this.labelPercent.AutoSize = true;
            this.labelPercent.Font = new System.Drawing.Font("Segoe UI", 11.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPercent.ForeColor = System.Drawing.Color.White;
            this.labelPercent.Location = new System.Drawing.Point(95, 2);
            this.labelPercent.Name = "labelPercent";
            this.labelPercent.Size = new System.Drawing.Size(41, 28);
            this.labelPercent.TabIndex = 5;
            this.labelPercent.Text = "0%";
            // 
            // panelPB
            // 
            this.panelPB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(116)))), ((int)(((byte)(133)))), ((int)(((byte)(145)))));
            this.panelPB.Controls.Add(this.panel1);
            this.panelPB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPB.Location = new System.Drawing.Point(0, 0);
            this.panelPB.Name = "panelPB";
            this.panelPB.Size = new System.Drawing.Size(240, 149);
            this.panelPB.TabIndex = 4;
            // 
            // pictureBox1
            // 
            this.BatteryIMG.BackColor = System.Drawing.Color.Transparent;
            this.BatteryIMG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BatteryIMG.Image = global::drc.Properties.Resources.battery0;
            this.BatteryIMG.Location = new System.Drawing.Point(10, 10);
            this.BatteryIMG.Name = "pictureBox1";
            this.BatteryIMG.Size = new System.Drawing.Size(220, 99);
            this.BatteryIMG.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BatteryIMG.TabIndex = 2;
            this.BatteryIMG.TabStop = false;
            // 
            // PercenBatt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelPB);
            this.Name = "PercenBatt";
            this.Size = new System.Drawing.Size(240, 149);
            this.Load += new System.EventHandler(this.PercenBatt_Load);
            this.panel1.ResumeLayout(false);
            this.panelLayout.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panelPercent.ResumeLayout(false);
            this.panelPercent.PerformLayout();
            this.panelPB.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BatteryIMG)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelLayout;
        private System.Windows.Forms.Panel panelPB;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox BatteryIMG;
        private System.Windows.Forms.Panel panelPercent;
        private System.Windows.Forms.Label labelPercent;

    }
}
